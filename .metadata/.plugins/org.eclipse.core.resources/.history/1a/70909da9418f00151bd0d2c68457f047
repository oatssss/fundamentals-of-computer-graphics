package comp557.a4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeMap;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Half edge data structure.
 * Maintains a list of faces (i.e., one half edge of each) to allow
 * for easy display of geometry.
 */
public class HEDS {

    /** List of faces */
    List<Face> faces = new ArrayList<Face>();
    
    /** Map of half edges */
    Map<String, HalfEdge> halfEdges = new TreeMap<String, HalfEdge>();
    
    /**
     * Constructs an empty mesh (used when building a mesh with subdivision)
     */
    public HEDS() {
        // do nothing
    }
        
    /**
     * Builds a half edge data structure from the polygon soup and calculates the Q values for the vertices
     * @param soup
     */
    public HEDS( PolygonSoup soup ) {
        halfEdges.clear();
        faces.clear();
        for ( int[] face : soup.faceList ) {
            try {
            	// Make the half edge between the 3rd and 1st vertex
                int i = face[face.length-1];
                int j = face[0];
                HalfEdge he = createHalfEdge( soup, i, j );
                HalfEdge first = he;                
                // Make the half edge between the 1st to 2nd, then 2nd to 3rd, and also link them to the HE for 3rd to 1st, then 1st to 2nd respectively
                for ( int index = 1; index < face.length; index++ ) {
                    i = j; 
                    j = face[index];
                    HalfEdge next = createHalfEdge( soup, i, j );
                    he.next = next;
                    he = next;
                }
                // Link the HE for 3rd to 1st to the HE for 2nd to 3rd
                he.next = first;
                faces.add( new Face(he) ); // assignment is redundant, but makes it clear what's happening
            } catch ( Exception e ) {
                System.out.println("bad face, starting with... " + face[0] + " " + face[1] + " " + face[2] );
                MeshSimplificationApp.setLogString("bad face, starting with... " + face[0] + " " + face[1] + " " + face[2]);
            }
        }
        
        // Calculate the Q values for each vertex
        calculateQValues();
        
        // Build the priority queue for edge collapsing
//        buildPriorityQueue();
    }
    
    private void calculateQValues() {
    	// Calculate Q for each vertex - since a vertex doesn't have any references to its connected faces
    	// we'll use the list of half edges to iterate through every vertex (since vertices are heads) and
    	// obtain the faces connected to every half edge's head. Since a vertex can be the head of multiple
    	// half edges, we only calculate its Q if it hasn't already been set, i.e. if it's still null.
    	for (HalfEdge halfEdge : halfEdges.values()) {
			Vertex v = halfEdge.head;
			if (v.Q == null) {	// Make sure we haven't already calculated Q for this vertex
				// Iterate through this vertex's connected half edges in a clockwise manner
				Matrix4d vertexQ = new Matrix4d(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
				HalfEdge connectedHE = halfEdge;
				boolean ranIntoBoundary = false;
				do {
					vertexQ.add(connectedHE.leftFace.K);
					connectedHE = connectedHE.next.twin;
					
					// Although I'm assuming that the mesh will not have edges that are boundaries
					// I don't want the program to crash if it does
					if (connectedHE == null)
						{ ranIntoBoundary = true; break; }
					
				} while (connectedHE != halfEdge);

				// Go in counter-clockwise order to cover all the faces connected to a vertex if a boundary was found
				if (ranIntoBoundary) {
					connectedHE = halfEdge;
					while (connectedHE.twin != null) {
						connectedHE = connectedHE.twin.prev();
						vertexQ.add(connectedHE.leftFace.K);
					}
				}
				
				// Finally set the Q of the vertex as the sum of the fundamental Ks
				v.Q = vertexQ;
			}
		}
    }
    
    public void buildPriorityQueue(PriorityQueue<Edge> queue, Double regularizationWeight) {
    	
    	queue.clear();
    	
    	for (HalfEdge halfEdge : halfEdges.values()) {			
			// Build the full edge that will be associated with the half edge
			Edge e = new Edge();
			e.he = halfEdge;
			halfEdge.e = e;
			
			// We're only going to add the edge to the priority queue if it's not already in there
			if (queue.contains(e))
			{
				// If the edge already exists, make sure this half edge references the existing one
				halfEdge.e = halfEdge.twin.e;
				continue;
			}
			
			// Compute the quadric errors of this edge
			e.recomputeQuadrics(regularizationWeight);
			
			// Add the edge to the priority queue
			queue.add(e);
		}
    }
    
    private HalfEdge createHalfEdge( PolygonSoup soup, int i, int j ) throws Exception {
        String p = i+","+j;
        if ( halfEdges.containsKey( p ) ){
            throw new Exception("non orientable manifold");
        }
        String twin = j+","+i;
        HalfEdge he = new HalfEdge();
        he.head = soup.vertexList.get(j);
        he.twin = halfEdges.get( twin );
        if ( he.twin != null ) he.twin.twin = he;
        halfEdges.put( p, he );        
        return he;        
    }
    
    // TODO: Objective 2, 3, 4, 5: write methods to help with collapse, and for checking topological problems
    
    /**
     * Collapses the edge associated with a half edge to a specified vertex 
     * @param collapsePoint The point to which the edge should collapse
     * @param halfEdge A half edge of the edge to collapse
     * @return true if the collapse was successful, false otherwise
     */
    public boolean collapse( Vector4d collapsePosition, HalfEdge halfEdge, PriorityQueue<Edge> queue, List<Edge> skipped, double regularizationWeight ) {

    	Vertex collapsePoint = new Vertex();
    	collapsePoint.p = new Point3d(collapsePosition.x, collapsePosition.y, collapsePosition.z);
    	collapsePoint.Q = halfEdge.e.Q;

    	// PROBLEM IS HERE, I'M FEEDING IT AN IMPROPER HALF EDGE, LOOK BELOW, I'M NOT REMOVING AND READING TO QUEUE PROPERLY
    	if (!edgeCollapseCausesProblems(halfEdge)) {
    		
    		// Remove the collapsing edge from the priority queue
    		queue.remove(halfEdge.e);
    		skipped.remove(halfEdge.e);
    	
	    	// Point the relevant half edges to the new collapse point
	    	HalfEdge collapsingHE;
	    	collapsingHE = halfEdge;
	    	redirectHeads(collapsePoint, collapsingHE);
	    	collapsingHE = halfEdge.twin;
	    	redirectHeads(collapsePoint, collapsingHE);
	    	
	    	boolean repeatedForTwin = false;
	    	collapsingHE = halfEdge;
	    	HalfEdge containedInMesh = null;
	    	do {
	    		if (collapsingHE != null) {
		    		removeFace(collapsingHE.leftFace);
			    	if (collapsingHE.next.twin == null || collapsingHE.prev().twin == null) {
						containedInMesh = setNewBoundaryForCollapse(collapsingHE, queue, skipped);
					}
			    	else {
						containedInMesh = mergePartnerHalfEdgesForCollapse(collapsingHE, queue, skipped);
					}
	    		}
		    	collapsingHE = collapsingHE.twin;
		    	repeatedForTwin = !repeatedForTwin;
			} while (repeatedForTwin);
	    	// Find a half edge contained in the mesh that has the collapse point as a vertex
	    	while (containedInMesh.head != collapsePoint) {
				containedInMesh = containedInMesh.next;
			}
	    	// Remove all the adjacent edges from the queue, update them, then put them back
	    	List<HalfEdge> adjacentHalfEdges = adjacentHalfEdgesToHead(containedInMesh);
	    	HashSet<Edge> adjacentEdges = new HashSet<Edge>();
	    	for (HalfEdge he : adjacentHalfEdges) { // TODO : PROPERLY IMPLEMENT THIS, IT'S GETTING A NULL POINTER ATM
//	    		if (he == null) System.out.println("HE NULL");
//	    		if (he.e == null) System.out.println("HE.E NULL");
//	    		if (adjacentEdges == null) System.out.println("LIST NULL");
				adjacentEdges.add(he.e);
			}
	    	
	    	queue.removeAll(adjacentEdges);
	    	skipped.removeAll(adjacentEdges);
	    	for (Edge edge : adjacentEdges) {
				edge.recomputeQuadrics(regularizationWeight);
			}
	    	queue.addAll(adjacentEdges);

	    	// Pressing C should collapse, then assign the current HE to one that didn't get obliterated, no need to handle that here	    	
	    	return true;
    	}
    	else {
    		String errorString = "Collapsing this edge will cause topological problems; ignoring...";
    		System.out.println(errorString); 
    		MeshSimplificationApp.setLogString(errorString);
    		return false;
    	}
    }
    
    /**
     * Finds the half edge that should become the new boundary after he is collapsed, then removes the edges that have
     * been obliterated from queue.
     * @param he The half edge that is collapsing
     * @param queue The priority queue from which edges that are obliterated should be removed from
     * @return The new boundary half edge.
     */
    private HalfEdge setNewBoundaryForCollapse( HalfEdge he, PriorityQueue<Edge> queue, List<Edge> skipped ) {
    	HalfEdge newBoundaryHE = he.next;
		if (newBoundaryHE.twin == null) {
			newBoundaryHE = newBoundaryHE.next;
		}
		newBoundaryHE.twin.twin = null;
		newBoundaryHE.twin.e.he = newBoundaryHE.twin;
		// Remove the edges that got obliterated from the priority queue
		Edge firstObliterated = newBoundaryHE.next.e;
		Edge secondObliterated = newBoundaryHE.prev().e;
		queue.remove(firstObliterated);
		queue.remove(secondObliterated);
		skipped.remove(firstObliterated);
		skipped.remove(secondObliterated);
		return newBoundaryHE.twin;
	}
    
    /**
     * Merges the other two half edges of the face together, then removes the collapse he from queue.
     * @param he The half edge that is collapsing, and from which next and prev will be merged.
     * @param queue The priority queue from which the collapsing edge should be removed from
     * @return A half edge that is for sure still part of the mesh.
     */
    private HalfEdge mergePartnerHalfEdgesForCollapse( HalfEdge he, PriorityQueue<Edge> queue, List<Edge> skipped ) {
    	// Have to update the edge that merges together as a result of the face disappearing
		// The merged edge makes he.next.twin and he.prev.twin twins to each other
		he.next.twin.twin = he.prev().twin;
		he.prev().twin.twin = he.next.twin;
		// Remove the edge that got collapsed from the queue, and also the edge that will get overridden
		queue.remove(he.e);
		queue.remove(he.prev().twin.e);
		skipped.remove(he.e);
		skipped.remove(he.prev().twin.e);
		// Update the whole edge
		he.next.twin.e.he = he.next.twin;
		he.prev().twin.e = he.next.twin.e;
		
		return he.next.twin;
    }
    
    private void redirectHeads( Vertex newHead, HalfEdge connectedHE ) {
    	List<HalfEdge> adjacentsPointingAway = adjacentHalfEdgesToHead(connectedHE);
    	for (HalfEdge halfEdge : adjacentsPointingAway) {
			if (halfEdge.twin != null) {
				halfEdge.twin.head = newHead;
			}
		}
    	/*boolean ranIntoBoundary = false;
    	HalfEdge l = connectedHE;
    	do {
			l.head = newHead;
			l = l.next.twin;
			
			// In case the cycling through adjacent edges bumps into a boundary
			if (l == null) {
				ranIntoBoundary = true;
				break;
			}
		} while (l != connectedHE);
    	
    	if (ranIntoBoundary) {
			// Cycle in reverse order to get all adjacent edges, since going one way didn't circle all the way back (due to boundary), we could have missed some
    		l = connectedHE.twin;
    		while (l != null) {
				l.prev().head = newHead;
				l = l.prev().twin;
			}
		}*/
    }
    
    private void removeFace( Face face ) {
    	// It seems that with the given solution to objective 1, there's
    	// no way to find the string key associated with a half edge, meaning
    	// there's no way to remove a specific half edge from the half edge map.
    	// I think this means that even though half edges won't be referenced
    	// from edges and faces themselves, they still won't get garbage collected
    	// because they're still referenced within the map. Maybe have the Vertex class
    	// hold its index in the polygon soup? Then the string key for a half edge
    	// can be reconstructed from its head and the previous's head.
    	
    	faces.remove(face);
    }
    
    private boolean edgeCollapseCausesProblems( HalfEdge collapsingEdge ) {

    	// Make sure the mesh isn't a tetrahedron
    	if (faces.size() <= 4)
    		{ return true; }
    	// Make sure there's only two common adjacent vertices
    	List<Vertex> headAdjacents = adjecentVerticesToHead(collapsingEdge);
    	List<Vertex> tailAdjacents = adjecentVerticesToHead(collapsingEdge.twin);
    	headAdjacents.retainAll(tailAdjacents);
    	if (headAdjacents.size() > 2)
    		{ return true; }
    	// Make sure boundary vertices get collapsed only on boundary edges
    	boolean headIsOnBoundary = isHeadOnBoundary(collapsingEdge);
    	boolean tailIsOnBoundary = isHeadOnBoundary(collapsingEdge.twin);
    	boolean edgeIsBoundary = collapsingEdge.twin == null;
    	if (headIsOnBoundary && tailIsOnBoundary && !edgeIsBoundary)
    		{ return true; }
    	return false;
	}
    
    /**
     * Finds all the half edges that have halfEdge.head as their tails
     * @param halfEdge The half edge who's head to use as the tail of the adjacents
     * @return All the half edges that are connected to halfEdge.head from the tail
     */
    private List<HalfEdge> adjacentHalfEdgesToHead( HalfEdge halfEdge ) {
    	List<HalfEdge> adjacents = new ArrayList<HalfEdge>();
    	
    	// Return an empty list if the half edge given does not exist
    	if (halfEdge == null)
    		{ return adjacents; }
    	
    	boolean ranIntoBoundary = false;
    	
    	HalfEdge heNext = halfEdge.next;
    	do {
			adjacents.add(heNext);
			heNext = heNext.twin;
			
			if (heNext == null) {
				ranIntoBoundary = true;
				break;
			}
			heNext = heNext.next;
			
		} while (heNext != halfEdge.next);
    	
    	if (ranIntoBoundary) {
			// Go the other way until we hit the boundary again
    		heNext = halfEdge.twin;
    		while (heNext != null) {
				adjacents.add(heNext);
				heNext = heNext.prev().twin;
			}
		}
    	
    	return adjacents;
    }
    
    private List<Vertex> adjecentVerticesToHead( HalfEdge halfEdge ) {
		List<HalfEdge> adjacentHalfEdgesToHead = adjacentHalfEdgesToHead(halfEdge);
		List<Vertex> adjacentVertices = new ArrayList<Vertex>();
		
		for (HalfEdge he : adjacentHalfEdgesToHead) {
			adjacentVertices.add(he.head);
		}
		
		return adjacentVertices;
	}
    
    private boolean isHeadOnBoundary( HalfEdge halfEdge ) {
    	boolean onBoundary = false;
    	
    	// If the head is on the boundery, we won't be able to cycle through its half edges, then get back to where we started because of a missing face
    	HalfEdge he = halfEdge;
    	while (he.next.twin != halfEdge) {
    		he = he.next.twin;
			if (he == null)
			{
				onBoundary = true;
				break;
			}
		}
    	
    	return onBoundary;
    }
    
    /**
     * Draws the half edge data structure by drawing each of its faces.
     * Per vertex normals are used to draw the smooth surface when available,
     * otherwise a face normal is computed. 
     * @param drawable
     */
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

        // we do not assume triangular faces here        
        Point3d p;
        Vector3d n;        
        for ( Face face : faces ) {
            HalfEdge he = face.he;
            gl.glBegin( GL2.GL_POLYGON );
            n = he.leftFace.n;
            gl.glNormal3d( n.x, n.y, n.z );
            HalfEdge e = he;
            do {
                p = e.head.p;
                gl.glVertex3d( p.x, p.y, p.z );
                e = e.next;
            } while ( e != he );
            gl.glEnd();
        }
    }

}
