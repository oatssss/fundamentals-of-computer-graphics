package comp557.a5;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Class for a plane at y=0.
 *
 * This surface can have two materials.  If both are defined, a 1x1 tile checker
 * board pattern should be generated on the plane using the two materials.
 */
public class Plane extends Intersectable {

	/** The second material, if non-null is used to produce a checker board pattern. */
	Material material2;

	/** The plane normal is the y direction */
	public static final Vector3d n = new Vector3d(0, 1, 0);

	/**
	 * Default constructor
	 */
	public Plane() {
		super();
	}


	@Override
	public void intersect( Ray ray, IntersectResult result ) {

		// TODO: Objective 4: finish this class
		
		/* Determine whether the ray intersects the plane */
		
		//	Plane is assumed to be y=0, thus the normal is the y-axis
		Vector3d normal = new Vector3d(0, 1, 0);
		//	Represent eyePoint as a Vector
		Vector3d p0 = new Vector3d(ray.eyePoint);
		
		//	Check ray is not parallel to plane
		if (ray.viewDirection.dot(normal) != 0) {
			double t = -(p0.dot(normal))/(ray.viewDirection.dot(normal));
			//	Negative t values mean the plane is behind the ray's eyePoint, therefore not visible
			//	Also, only update the result if this plane is the closest object found so far 
			if (t < 0 /*|| t > result.t*/)
				{ return; }
			else {
				//	Calculate the actual intersection point using t
				Point3d td = new Point3d(ray.viewDirection);
				td.scale(t);
				Point3d p = new Point3d(p0);
				p.add(td);
				
				//	Determine which material the point is on
				Material finalMaterial;
				boolean xEven = (((int) Math.ceil(p.x)) % 2) == 0 ? true : false;
				boolean zEven = (((int) Math.ceil(p.z)) % 2) == 0 ? true : false;
				if (xEven == zEven)
					{ finalMaterial = this.material; }
				else
					{ finalMaterial = this.material2; }
				
				// Update result
				result.n.set(normal);
				result.p.set(p);
				result.t = t;
				result.material = finalMaterial;
			}
		}
	}
}
