package comp557.a2;

import mintools.parameters.DoubleParameter;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

public class DAGJointFree extends DAGNode {
	
	DoubleParameter offsetX = new DoubleParameter("Offset X", 0, -2, 2);
	DoubleParameter offsetY = new DoubleParameter("Offset Y", 0, -2, 2);
	DoubleParameter offsetZ = new DoubleParameter("Offset Z", 0, -2, 2);
	DoubleParameter xRot = new DoubleParameter("X Rotation (Degrees)", 0, -180, 180);
	DoubleParameter yRot = new DoubleParameter("Y Rotation (Degrees)", 0, -180, 180);
	DoubleParameter zRot = new DoubleParameter("Z Rotation (Degrees)", 0, -180, 180);
	
	public DAGJointFree ( String name ) {
		this.name = name;
		this.dofs.add(offsetX);
		this.dofs.add(offsetY);
		this.dofs.add(offsetZ);
		this.dofs.add(xRot);
		this.dofs.add(yRot);
		this.dofs.add(zRot);
	}
	
	@Override
	public void display( GLAutoDrawable drawable ) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glPushMatrix();
    	gl.glScalef(0.1f, 0.1f, 0.1f);
		gl.glTranslatef(offsetX.getFloatValue(), offsetY.getFloatValue(), offsetZ.getFloatValue());
		gl.glRotatef(xRot.getFloatValue(), 1, 0, 0);
		gl.glRotatef(yRot.getFloatValue(), 0, 1, 0);
		gl.glRotatef(zRot.getFloatValue(), 0, 0, 1);
		super.display(drawable);
		gl.glPopMatrix();
	}
}
