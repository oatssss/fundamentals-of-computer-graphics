This program displays how a half-edge data structure can be used to represent a 3D model
(contained in an .obj file). Follow the on screen instructions and make use of the controls
made available in the control panel.