package comp557.a5;

import java.awt.Dimension;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Simple camera object, which could be extended to handle a variety of
 * different camera settings (e.g., aperature size, lens, shutter)
 */
public class Camera {

	/** Camera name */
	public String name = "camera";

	/** The eye position */
	public Point3d from = new Point3d(0,0,10);

	/** The "look at" position */
	public Point3d to = new Point3d(0,0,0);

	/** Up direction, default is y up */
	public Vector3d up = new Vector3d(0,1,0);

	/** Vertical field of view (in degrees), default is 45 degrees */
	public double fovy = 45.0;

	/** The rendered image size */
	public Dimension imageSize = new Dimension(640,480);
	
	/** The transformation matrix that can be applied to points and vectors to get they're representation in world space */
	private Matrix4d localToWorld = null;
	
	/**
	 * Default constructor
	 */
	public Camera() {
		// do nothing
	}
	
	public Matrix4d localToWorldTransformMatrix() {
		
		// Either calculate or pull from cache
		if (localToWorld == null) {
			Matrix4d transform = new Matrix4d();
			
			/* Find the world space rotation of the camera as a rotation matrix */	
			Vector3d rotRow0 = new Vector3d();
			Vector3d rotRow1 = new Vector3d();
			Vector3d rotRow2 = new Vector3d();
			
			Vector3d zAxisLocal = new Vector3d();
			zAxisLocal.sub(this.from, this.to);
			zAxisLocal.normalize();
			
			Vector3d xAxisLocal = new Vector3d();
			xAxisLocal.cross(this.up, zAxisLocal);
			xAxisLocal.normalize();
			
			Vector3d yAxisLocal = new Vector3d();
			yAxisLocal.cross(xAxisLocal, zAxisLocal);
			yAxisLocal.normalize();
			
			rotRow0.set(xAxisLocal.x, yAxisLocal.x, zAxisLocal.x);
			rotRow1.set(xAxisLocal.y, yAxisLocal.y, zAxisLocal.y);
			rotRow2.set(xAxisLocal.z, yAxisLocal.z, zAxisLocal.z);
			
			Matrix3d cameraRot = new Matrix3d();
			cameraRot.setRow(0, rotRow0);
			cameraRot.setRow(1, rotRow1);
			cameraRot.setRow(2, rotRow2);
			
			/* Set up the final transform matrix */
			transform.setRotationScale(cameraRot);
			transform.setTranslation(new Vector3d(this.from));
			
			localToWorld = transform;
			return localToWorld;
		}
		else {
			return localToWorld;
		}
	}
}
