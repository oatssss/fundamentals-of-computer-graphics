package comp557.a4;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

/**
 * Simple face class
 */
public class Face {    
    
	/** sure, why not keep a normal for flat shading? */
    public Vector3d n = new Vector3d();

    /** Plane equation */
	Vector4d p = new Vector4d();

    /** Quadratic function for the plane equation */
    public Matrix4d K = new Matrix4d();

    /** Some half edge on the face */
    HalfEdge he;
    
    /** 
     * Constructs a face from a half edge, and computes the flat normal
     * @param he
     */
    public Face( HalfEdge he ) {
        this.he = he;
        HalfEdge loop = he;
        do {
            loop.leftFace = this;
            loop = loop.next;
        } while ( loop != he );
        recomputeNormal();
    }
    
    public void recomputeNormal() {
    	Point3d p0 = he.head.p;
        Point3d p1 = he.next.head.p;
        Point3d p2 = he.next.next.head.p;
        Vector3d v1 = new Vector3d();
        Vector3d v2 = new Vector3d();
        v1.sub(p1,p0);
        v2.sub(p2,p1);
        n.cross( v1,v2 );
        
        // Compute the plane equation
        // Plane equation: [a(x-x_0) + b(y-y_0) + c(z-z_0) = 0]
        // Rewritten:      [ax + by + cz - ax_0 - by_0 - cz_0 = 0] , (x_0,y_0,z_0) is a point on the plane, (a,b,c) is the plane normal

        // According to the paper, (a^2+b^2+c^2) = 1, so we'll normalize them in that way here (not sure if necessary)
        double a = n.x;
        double b = n.y;
        double c = n.z;
        double divisor = Math.pow(a, 2) + Math.pow(b, 2) + Math.pow(c, 2);
        a = a/Math.sqrt(divisor);
        b = b/Math.sqrt(divisor);
        c = c/Math.sqrt(divisor);
        double d = -(a*he.head.p.x + b*he.head.p.y + c*he.head.p.z);
        p.set(a, b, c, d);
        
        // Compute K
        K.setRow(0, a*a, a*b, a*c, a*d);
        K.setRow(1, a*b, b*b, b*c, b*d);
        K.setRow(2, a*c, b*c, c*c, c*d);
        K.setRow(3, a*d, b*d, c*d, d*d);
    }
    
}
