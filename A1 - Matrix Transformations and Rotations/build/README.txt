Use the the tabs to choose between different rotational control schemes.
Do your best to try and match the left set of axes to the orientation on the right.
Once matched closely enough, the axes will be circled green.