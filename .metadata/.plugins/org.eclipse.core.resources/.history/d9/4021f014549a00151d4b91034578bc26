package comp557.a5;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Simple scene loader based on XML file format.
 */
public class Scene {

	/**
	 * Flat array of surfaces
	 */
	public Intersectable root;

	/**
	 * All scene lights.
	 */
	public Map<String,Light> lights;

	/**
	 * Contains information about how to render the scene.
	 */
	public Render render;

	/**
	 * The ambient colour.
	 */
	public Color3f ambient;

	/**
	 * Default constructor.
	 */
	public Scene() {
		this.root = new SceneNode();
		this.render = new Render();
		this.ambient = new Color3f();
		this.lights = new HashMap<String,Light>();
	}

	/**
	 * renders the scene
	 */
	public void render(boolean showPanel) {

		Camera cam = render.camera;
		int w = cam.imageSize.width;
		int h = cam.imageSize.height;

		render.init(w, h, showPanel);
		
		//	Supersample using a grid pattern + jitter
		int gridCells = nearestPowerOf2(render.samples);
		int rowSize = (int) (Math.log(gridCells)/Math.log(2));
		int colSize = rowSize;	//	Grid's a square

		for ( int i = 0; i < h && !render.isDone(); i++ ) {
			for ( int j = 0; j < w && !render.isDone(); j++ ) {
				
				Ray ray = new Ray();
				double[] offset = new double[2];	//	Offset coordinate values range from [0 1], (0,0) is bottom left
				double rCumulative = 0, g = 0, b = 0;
				int argb = 0;
				
				//	Do the super sampling
				for (int k = 0; k < rowSize; k++) {
					offset[0] = k/(double)rowSize + (1/(double)rowSize)/2;
					for (int l = 0; l < colSize; l++) {
						offset[1] = l/(double)colSize + (1/(double)colSize)/2;
						
						generateRay(i, j, offset, cam, ray);
//						generateRay(i, j, new double[] {0.5,0.5}, cam, ray);

						IntersectResult result = new IntersectResult();
						result.t = Double.POSITIVE_INFINITY;
						this.root.intersect(ray, result);
						
						Color3f c = new Color3f(render.bgcolor);
						if (result.t < Double.POSITIVE_INFINITY) {
							
							// Hold colour values for each channel
							float red = 0, green = 0, blue = 0;
							
							// Calculate contribution of each light
							for (Light light : this.lights.values()) {
								
								Ray shadowRay = new Ray();
								IntersectResult shadowResult = new IntersectResult();
								Vector3d lightVector = new Vector3d();
								lightVector.sub(light.from, result.p);
								lightVector.normalize();
								
								//	Only do light calculations if this light isn't occluded at this pixel
								if (!inShadow(result, light, root, shadowResult, shadowRay)) {
									
									/* Do lambertian */
									//	Get n dot light
									double nDotl = result.n.dot(lightVector);
									//	Calculate the individual colours
									red += (float) (result.material.diffuse.x*light.color.x*light.power*Math.max(0, nDotl));
									green += (float) (result.material.diffuse.y*light.color.y*light.power*Math.max(0, nDotl));
									blue += (float) (result.material.diffuse.z*light.color.z*light.power*Math.max(0, nDotl));
									
									/* Do specular phong */
									Vector3d lookVector = new Vector3d(ray.viewDirection);
									lookVector.negate();
									Vector3d halfVector = new Vector3d();
									halfVector.add(lightVector, lookVector);
									halfVector.normalize();
									double nDoth = result.n.dot(halfVector);
									red += (float) (result.material.specular.x*light.color.x*light.power*Math.pow(Math.max(0, nDoth), result.material.hardness));
									green += (float) (result.material.specular.y*light.color.y*light.power*Math.pow(Math.max(0, nDoth), result.material.hardness));
									blue += (float) (result.material.specular.z*light.color.z*light.power*Math.pow(Math.max(0, nDoth), result.material.hardness));
								}
								
								/* Always do ambient */
								red += (float) (result.material.diffuse.x*this.ambient.x);
								green += (float) (result.material.diffuse.y*this.ambient.y);
								blue += (float) (result.material.diffuse.z*this.ambient.z);
							}
							
							red = Math.min(1, red);
							green = Math.min(1, green);
							blue = Math.min(1, blue);
							c.set(red, green, blue);
							
							rCumulative += (int)(255*c.x);
							g += (int)(255*c.y);
							b += (int)(255*c.z);
						}
						
						rCumulative /= render.samples;
						g /= render.samples;
						b /= render.samples;
						
//						int prevR = (argb<<8)>>>24; prevR = prevR == 0 ? r : prevR;
//						int prevG = (argb<<16)>>>24; prevG = prevG == 0 ? g : prevG;
//						int prevB = (argb<<24)>>>24; prevB = prevB == 0 ? b : prevB;
//						
//						r = (int) Math.round((prevR + r)/(double)2);
//						g = (int) Math.round((prevG + g)/(double)2);
//						b = (int) Math.round((prevB + b)/(double)2);
						
						int a = 255;
//						argb = (a<<24 | r<<16 | g<<8 | b);
					}
				}

				// update the render image
				render.setPixel(j, i, argb);
			}
		}

		// save the final render image
		render.save();

		// wait for render viewer to close
		render.waitDone();

	}
	
	/**
	 * Round a number to the nears power of 2
	 * @param aNumber The number to round
	 * @return aNumber's nearest power of 2
	 */
	private int nearestPowerOf2(int aNumber) {
		return (int) Math.pow(2, Math.round(Math.log(aNumber) / Math.log(2)));
	}

	/**
	 * Generate a ray through pixel (i,j).
	 *
	 * @param i The pixel row.
	 * @param j The pixel column.
	 * @param offset The offset from the center of the pixel, in the range [-0.5,+0.5] for each coordinate.
	 * @param cam The camera.
	 * @param ray Contains the generated ray.
	 */
	public static void generateRay(final int i, final int j, final double[] offset, final Camera cam, Ray ray) {
		
		// Calculate left, right, top, bottom for a screen 1 unit away from the camera
		double d = 1.0;
		double t = Math.tan(cam.fovy/2);
		double b = -t;
		double r = (t * (cam.imageSize.getWidth()/cam.imageSize.getHeight()));
		double l = -r;
		
		// Get the width and length of one pixel
		double pixWidth = (r - l)/cam.imageSize.getWidth();
		double pixLength = (t - b)/cam.imageSize.getHeight();
		
		// Calculate the offset position with respect to a central pixel
		double offsetX = offset[0] - 0.5;	// Center values
		double offsetY = offset[1] - 0.5;
		
		// Find the position of the (i,j) pixel in local coordinates
		double u = l + (j + 0.5)*pixWidth + offsetX*pixWidth;
		double v = b + (i + 0.5)*pixLength + offsetY*pixLength;
		Point3d pixelPos = new Point3d(u, v, -d);
		
		// Find the position of the (i,j) pixel in world space coordinates
		cam.localToWorldTransformMatrix().transform(pixelPos);
		
		// Get the direction of the ray as a normalized vector from the origin
		Vector3d rayDirection = new Vector3d();
		rayDirection.sub(pixelPos, cam.from);
		rayDirection.normalize();
		
		// Output the ray
		ray.set(cam.from, rayDirection);
	}

	/**
	 * Shoot a shadow ray in the scene and get the result.
	 *
	 * @param result Intersection result from raytracing.
	 * @param light The light to check for visibility.
	 * @param surfaces List of intersectable surfaces
	 * @param shadowResult Contains the result of a shadow ray test.
	 * @param shadowRay Contains the shadow ray used to test for visibility.
	 *
	 * @return True if a point is in shadow, false otherwise.
	 */
	public static boolean inShadow(final IntersectResult result, final Light light, final Intersectable root, IntersectResult shadowResult, Ray shadowRay) {

		/* First find out if this light is occluded at this pixel */
		double epsilon = 0.000001;	//	Set an arbitrary epsilon
		//	Get normalized light vector
		Vector3d lightVectorUnnormalized = new Vector3d();
		lightVectorUnnormalized.sub(light.from, result.p);
		Vector3d lightVector = new Vector3d(lightVectorUnnormalized);
		lightVector.normalize();
		//	Offset the shadow surface by a tiny amount towards the light
		Point3d shadowRayStart = new Point3d(result.p);
		Vector3d shadowRayOffset = new Vector3d(lightVector);
		shadowRayOffset.scale(epsilon);
		shadowRayStart.add(shadowRayOffset);
		//	Generate the shadow ray
		shadowRay.eyePoint = shadowRayStart;
		shadowRay.viewDirection = new Vector3d(lightVector);
		//	Calculate the max t the ray may acquire (past this the shadow ray goes beyond the light)
		double tMax = lightVectorUnnormalized.length();
		//	Intersect the ray with the scene
		shadowResult.t = Double.POSITIVE_INFINITY;
		root.intersect(shadowRay, shadowResult);

		return shadowResult.t <= tMax;
	}
}
