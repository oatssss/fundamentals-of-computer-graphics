uniform float shininess;
uniform float ambient;

varying vec3 normal;  // surface normal in eye coordinates 
varying vec3 vertexPos;  // surface fragment location in eye coordinates
 
void main(void) {
	vec3 lightVector = normalize( gl_LightSource[0].position.xyz - vertexPos ); // vector direction towards light
	vec3 cameraVector = normalize( -vertexPos ); // vector direction towards camera
    vec3 halfVector = normalize( lightVector + cameraVector );
    vec3 ecnNorm = normalize( normal );
    
	float lambertian = max( 0.0, dot( ecnNorm, lightVector ) );
	float specular = 0.0;
	
	// Only allow specular lighting for lit surfaces
	if (lambertian > 0.0)
		{ specular = pow( max( 0.0, dot( ecnNorm, halfVector ) ) , shininess ); }
    
    gl_FragColor = vec4(
    					vec3(gl_FrontMaterial.diffuse) * lambertian * vec3(gl_LightSource[0].diffuse) + 
    					vec3(gl_FrontMaterial.specular) * specular * vec3(gl_LightSource[0].specular) + 
    					vec3(gl_FrontMaterial.ambient) * ambient * vec3(gl_LightSource[0].ambient), 1.0);
}
