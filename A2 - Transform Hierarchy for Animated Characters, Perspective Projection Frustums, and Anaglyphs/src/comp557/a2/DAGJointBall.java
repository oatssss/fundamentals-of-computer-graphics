package comp557.a2;

import javax.vecmath.Vector3d;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

import mintools.parameters.DoubleParameter;

public class DAGJointBall extends DAGNode {
	
	Vector3d offsetFromParent;
	DoubleParameter offsetX = new DoubleParameter("Offset X", 0, -10, 10);
	DoubleParameter offsetY = new DoubleParameter("Offset Y", 0, -10, 10);
	DoubleParameter offsetZ = new DoubleParameter("Offset Z", 0, -10, 10);
	DoubleParameter xRotGlobal = new DoubleParameter( "X Rotation (Degrees)", 0, -180, 180 );
	DoubleParameter yRotGlobal = new DoubleParameter( "Y Rotation (Degrees)", 0, -180, 180 );
	DoubleParameter zRotGlobal = new DoubleParameter( "Z Rotation (Degrees)", 0, -180, 180 );
	private boolean isBeingPlaced = false;
	
	public DAGJointBall ( String name, Vector3d offsetFromParent ) {
		super();
		this.name = name;
		this.dofs.add(this.xRotGlobal);
		this.dofs.add(this.yRotGlobal);
		this.dofs.add(this.zRotGlobal);
		this.offsetFromParent = offsetFromParent;
	}
	
	public DAGJointBall ( String name ) {
		this(name, new Vector3d(0, 0, 0));
		this.dofs.add(this.offsetX);
		this.dofs.add(this.offsetY);
		this.dofs.add(this.offsetZ);
//		this.dofs.add(this.xRotLocal);
//		this.dofs.add(this.yRotLocal);
//		this.dofs.add(this.zRotLocal);
		isBeingPlaced = true;
	}
	
	@Override
	public void display( GLAutoDrawable drawable ) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glPushMatrix();
		
		if (isBeingPlaced) {
			gl.glTranslatef(offsetX.getFloatValue(), offsetY.getFloatValue(), offsetZ.getFloatValue());
		}
		else {
			gl.glTranslatef((float)offsetFromParent.x, (float)offsetFromParent.y, (float)offsetFromParent.z);
		}
		
		/*gl.glMultMatrixd( M.asArray(), 0 );
		
		if (isBeingPlaced) {
			gl.glRotatef(xRotLocal.getFloatValue(), 1, 0, 0);
			gl.glRotatef(yRotLocal.getFloatValue(), 0, 1, 0);
			gl.glRotatef(zRotLocal.getFloatValue(), 0, 0, 1);
		}
		else {*/
			gl.glRotatef(xRotGlobal.getFloatValue(), 1, 0, 0);
			gl.glRotatef(yRotGlobal.getFloatValue(), 0, 1, 0);
			gl.glRotatef(zRotGlobal.getFloatValue(), 0, 0, 1);
//		}
		
		super.display(drawable);
		gl.glPopMatrix();
	}
}
