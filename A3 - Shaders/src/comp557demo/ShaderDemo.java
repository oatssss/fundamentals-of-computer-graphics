package comp557demo;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import mintools.parameters.BooleanParameter;
import mintools.parameters.DoubleParameter;
import mintools.parameters.Vec3Parameter;
import mintools.swing.VerticalFlowPanel;
import mintools.viewer.EasyViewer;
import mintools.viewer.SceneGraphNode;
import mintools.viewer.TrackBallCamera;

import com.jogamp.opengl.DebugGL2;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.glsl.ShaderCode;
import com.jogamp.opengl.util.glsl.ShaderProgram;
import com.jogamp.opengl.util.glsl.ShaderState;

public class ShaderDemo implements GLEventListener, SceneGraphNode {
	
    int viewingMode = 1;
    int previousViewingMode = viewingMode;
    
    /** Main trackball will come from the EasyViewer */
    TrackBallCamera tbc;
    
	GLU glu = new GLU();
	GLUT glut = new GLUT();
	
	boolean usePerFragment = false;
	boolean useCheckerboard = false;
	boolean useWoodcut = false;
	boolean useToon = false;
	
	ShaderState statePerFragment = new ShaderState();
	ShaderState stateCheckerboard = new ShaderState();
	ShaderState stateWoodcut = new ShaderState();
	ShaderState stateToon = new ShaderState();
	
	/** Adjustable parameters */
	// Vec3Parameter(Name, defaultX, defaultY, defaultZ)
	private Vec3Parameter lightPos = new Vec3Parameter("Light Position", 5, 5, 5);
	// DoubleParameter(Name, default, min, max)
	private DoubleParameter lightColR = new DoubleParameter("r", 1, 0, 1);
	private DoubleParameter lightColG = new DoubleParameter("g", 1, 0, 1);
	private DoubleParameter lightColB = new DoubleParameter("b", 1, 0, 1);
	private DoubleParameter ambient = new DoubleParameter( "Ambient Intensity", 0.1, 0, 1 );
	
	// Blinn-Phong parameters
	private DoubleParameter shininess = new DoubleParameter( "Shininess", 32, 1, 127 );
	// Checkerboard parameters
	private DoubleParameter col1R = new DoubleParameter("r", 1, 0, 1);
	private DoubleParameter col1G = new DoubleParameter("g", 1, 0, 1);
	private DoubleParameter col1B = new DoubleParameter("b", 1, 0, 1);
	private DoubleParameter col2R = new DoubleParameter("r", 0, 0, 1);
	private DoubleParameter col2G = new DoubleParameter("g", 0, 0, 1);
	private DoubleParameter col2B = new DoubleParameter("b", 0, 0, 1);
	private DoubleParameter frequency = new DoubleParameter("Checkerboard Frequency", 3, 1, 32);
	private BooleanParameter useIntFrequency = new BooleanParameter("Round Frequency", true);
	private BooleanParameter useAveraging = new BooleanParameter( "Use Averaging", true );
	private BooleanParameter useSmoothStep = new BooleanParameter( "Use Smooth Step", true );
	// Woodcut parameters
	private DoubleParameter time = new DoubleParameter("Time", 0, 0, 16);
	// Toon parameters
	private DoubleParameter thresholdHigh = new DoubleParameter("High Threshold", 0.95, 0, 1);
	private DoubleParameter thresholdMedium = new DoubleParameter("Medium Threshold", 0.5, 0, 1);
	private DoubleParameter thresholdLow = new DoubleParameter("Low Threshold", 0.25, 0, 1);
	
	// Store a reference to the EasyViewer
	private EasyViewer ev;
	
	private final String defaultSceneTab = "Scene";
	private final String blinnPhongTabName = "Blinn-Phong";
	private final String checkerboardTabName = "Checkerboard";
	private final String woodcutTabName = "Woodcut";
	private final String toonTabName = "Toon";
	
	VerticalFlowPanel blankPanel = new VerticalFlowPanel();
	VerticalFlowPanel lightControls = new VerticalFlowPanel();
	VerticalFlowPanel blinnPhongControls = new VerticalFlowPanel();
	VerticalFlowPanel checkerboardControls = new VerticalFlowPanel();
	VerticalFlowPanel woodcutControls = new VerticalFlowPanel();
	VerticalFlowPanel toonControls = new VerticalFlowPanel();
	
	public static void main( String[] args ) {
		new ShaderDemo();	
	}
	
	public ShaderDemo() {
		String name = "GLSL Shader Demo - Othniel Cundangan - 260575092";
		Dimension size = new Dimension(640, 480);
		Dimension controlSize = new Dimension(640, 640);
		ev = new EasyViewer(name, this, size, controlSize);
		tbc = ev.trackBall;	// Obtain the trackball from the EasyViewer
		addControls(); // Add the parameters that the user can adjust
		
		// Set up the toggling events for the shaders
		ev.glCanvas.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() >= KeyEvent.VK_1 && e.getKeyCode() <= KeyEvent.VK_4) {
                	previousViewingMode = viewingMode;
                    viewingMode = e.getKeyCode() - KeyEvent.VK_1 + 1;
                	if (viewingMode == 1)
                	{
                		if (previousViewingMode == viewingMode) usePerFragment = !usePerFragment;
                		else usePerFragment = true;
                		
                		useCheckerboard = false;
                		useWoodcut = false;
                		useToon = false;
                	}
                	else if (viewingMode == 2)
            		{
                		if (previousViewingMode == viewingMode) useCheckerboard = !useCheckerboard;
                		else useCheckerboard = true;
                		
                		usePerFragment = false;
                		useWoodcut = false;
                		useToon = false;
            		}
                	else if (viewingMode == 3)
            		{
                		if (previousViewingMode == viewingMode) useWoodcut = !useWoodcut;
                		else useWoodcut = true;
                		
                		usePerFragment = false;
                		useCheckerboard = false;
                		useToon = false;
            		}
                	else if (viewingMode == 4)
            		{
                		if (previousViewingMode == viewingMode) useToon = !useToon;
                		else useToon = true;
                		
                		usePerFragment = false;
                		useCheckerboard = false;
                		useWoodcut = false;
            		}
                	
                	// Set the control frame tab to the appropriate panel
                	if (usePerFragment)
            		{
                		ev.controlFrame.setSelectedTab(blinnPhongTabName);
                		blinnPhongControls.add(lightControls.getPanel());
            		}
                	else if (useCheckerboard)
            		{
                		ev.controlFrame.setSelectedTab(checkerboardTabName);
            		}
                	else if (useWoodcut)
                	{
                		ev.controlFrame.setSelectedTab(woodcutTabName);
                		woodcutControls.add(lightControls.getPanel());
                	}
                	else if (useToon)
                	{
                		ev.controlFrame.setSelectedTab(toonTabName);
                		toonControls.add(lightControls.getPanel());
                	}
                	else
                	{
                		ev.controlFrame.setSelectedTab(defaultSceneTab);
                		blankPanel.add(lightControls.getPanel());
					}
                }           
            }
        });
		
		tbc.attach(ev.glCanvas); // Allow the trackball to receive click/drag events from the EasyViewer's canvas
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		drawable.setGL( new DebugGL2( drawable.getGL().getGL2() ) );
		GL2 gl = drawable.getGL().getGL2();

        gl.glClearColor( 0, 0, 0, 1 );  	// Black Background
        gl.glClearDepth( 1 );          		// Depth Buffer Setup
        gl.glEnable(GL.GL_DEPTH_TEST);		// Enables Depth Testing
        gl.glDepthFunc(GL.GL_LEQUAL);		// The Type Of Depth Testing To Do

        gl.glEnable( GL2.GL_LIGHTING );
        gl.glEnable( GL2.GL_LIGHT0 );
        gl.glEnable( GL2.GL_NORMALIZE );	// normalize normals before lighting
       
        // Setup perFragmentLighting shader
        String shaderNamePerFragment = "perFragmentLighting";
        ShaderCode vsCodePerFragment = ShaderCode.create( gl, GL2.GL_VERTEX_SHADER, this.getClass(), "shaders", "shader/bin", shaderNamePerFragment, false );
        ShaderCode fsCodePerFragment = ShaderCode.create( gl, GL2.GL_FRAGMENT_SHADER, this.getClass(), "shaders", "shader/bin", shaderNamePerFragment, false );	
        ShaderProgram programPerFragment = new ShaderProgram();
        programPerFragment.add( vsCodePerFragment );
        programPerFragment.add( fsCodePerFragment );
		if ( !programPerFragment.link(gl, System.err) ) {
			throw new GLException("Couldn't link program: " + programPerFragment );
		}	
		statePerFragment.attachShaderProgram( gl, programPerFragment, false );
		
		// Setup perVertexLighting shader
		String shaderNameCheckerboard = "checkerboard";
        ShaderCode vsCodeCheckerboard = ShaderCode.create( gl, GL2.GL_VERTEX_SHADER, this.getClass(), "shaders", "shader/bin", shaderNameCheckerboard, false );
        ShaderCode fsCodeCheckerboard = ShaderCode.create( gl, GL2.GL_FRAGMENT_SHADER, this.getClass(), "shaders", "shader/bin", shaderNameCheckerboard, false );	
        ShaderProgram programCheckerboard = new ShaderProgram();
        programCheckerboard.add( vsCodeCheckerboard );
        programCheckerboard.add( fsCodeCheckerboard );
		if ( !programCheckerboard.link(gl, System.err) ) {
			throw new GLException("Couldn't link program: " + programCheckerboard );
		}	
		stateCheckerboard.attachShaderProgram( gl, programCheckerboard, false );
		
		// Setup woodcut shader
		String shaderNameWoodcut = "woodcut";
        ShaderCode vsCodeWoodcut = ShaderCode.create( gl, GL2.GL_VERTEX_SHADER, this.getClass(), "shaders", "shader/bin", shaderNameWoodcut, false );
        ShaderCode fsCodeWoodcut = ShaderCode.create( gl, GL2.GL_FRAGMENT_SHADER, this.getClass(), "shaders", "shader/bin", shaderNameWoodcut, false );	
        ShaderProgram programWoodcut = new ShaderProgram();
        programWoodcut.add( vsCodeWoodcut );
        programWoodcut.add( fsCodeWoodcut );
		if ( !programWoodcut.link(gl, System.err) ) {
			throw new GLException("Couldn't link program: " + programWoodcut );
		}	
		stateWoodcut.attachShaderProgram( gl, programWoodcut, false );
		
		// Setup toon shader
		String shaderNameToon = "toon";
        ShaderCode vsCodeToon = ShaderCode.create( gl, GL2.GL_VERTEX_SHADER, this.getClass(), "shaders", "shader/bin", shaderNameToon, false );
        ShaderCode fsCodeToon = ShaderCode.create( gl, GL2.GL_FRAGMENT_SHADER, this.getClass(), "shaders", "shader/bin", shaderNameToon, false );	
        ShaderProgram programToon = new ShaderProgram();
        programToon.add( vsCodeToon );
        programToon.add( fsCodeToon );
		if ( !programToon.link(gl, System.err) ) {
			throw new GLException("Couldn't link program: " + programToon );
		}	
		stateToon.attachShaderProgram( gl, programToon, false );
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        
        gl.glShadeModel( GL2.GL_SMOOTH ); // Enable Smooth Shading (Gouraud)
        
        // Display the scene according to the current trackball orientation, scale the scene to fit better
        tbc.prepareForDisplay(drawable);
        gl.glScaled( 3, 3, 3 );
        
        // Add a light
		int lightNumber = 0;
		float[] position = { lightPos.x, lightPos.y, lightPos.z, 1 };
        float[] colour = { lightColR.getFloatValue(), lightColG.getFloatValue(), lightColB.getFloatValue(), 1 };
        float[] acolour = { colour[0]*ambient.getFloatValue(), colour[1]*ambient.getFloatValue(), colour[2]*ambient.getFloatValue() };
        gl.glLightfv( GL2.GL_LIGHT0 + lightNumber, GL2.GL_SPECULAR, colour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0 + lightNumber, GL2.GL_DIFFUSE, colour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0 + lightNumber, GL2.GL_AMBIENT, acolour, 0 );
        gl.glLightfv( GL2.GL_LIGHT0 + lightNumber, GL2.GL_POSITION, position, 0 ); // transformed by the modelview matrix when glLight is called
        gl.glEnable( GL2.GL_LIGHT0 + lightNumber );
        
        // Determine which shader to display the scene with
        
        ShaderState customShader = null;
        
        if (viewingMode == 1)
        {
        	stateCheckerboard.useProgram( gl, false );
        	stateWoodcut.useProgram( gl, false );
        	stateToon.useProgram(gl, false);
        	statePerFragment.useProgram( gl, usePerFragment );
        	if (usePerFragment) customShader = statePerFragment;
        	else customShader = null;
    	}
        else if (viewingMode == 2)
        {
        	statePerFragment.useProgram( gl, false );
        	stateWoodcut.useProgram( gl, false );
        	stateToon.useProgram(gl, false);
        	stateCheckerboard.useProgram( gl, useCheckerboard );
        	if (useCheckerboard) customShader = stateCheckerboard;
        	else customShader = null;
    	}
        else if (viewingMode == 3)
        {
        	statePerFragment.useProgram( gl, false );
        	stateCheckerboard.useProgram( gl, false );
        	stateToon.useProgram(gl, false);
        	stateWoodcut.useProgram( gl, useWoodcut );
        	if (useWoodcut) customShader = stateWoodcut;
        	else customShader = null;
    	}
        else if (viewingMode == 4)
        {
        	statePerFragment.useProgram( gl, false );
        	stateCheckerboard.useProgram( gl, false );
        	stateWoodcut.useProgram( gl, false );
        	stateToon.useProgram(gl, useToon);
        	if (useToon) customShader = stateToon;
        	else customShader = null;
    	}
        
        // Setup the uniform values that may be used within the current shader
        if (customShader != null)
        {
        	// Blinn-Phong uniforms
	        int specExponentUniformLocation = customShader.getUniformLocation(gl, "shininess");
	        gl.glUniform1f(specExponentUniformLocation, shininess.getFloatValue());
	        int ambientUniformLocation = customShader.getUniformLocation(gl, "ambient");
	        gl.glUniform1f(ambientUniformLocation, ambient.getFloatValue());
	        
	        // Checkerboard uniforms
	        int colour1UniformLocation = customShader.getUniformLocation(gl, "Color1");
	        float[] colour1 = { col1R.getFloatValue(), col1G.getFloatValue(), col1B.getFloatValue() };
	        gl.glUniform3fv(colour1UniformLocation, 1, colour1, 0);
	        int colour2UniformLocation = customShader.getUniformLocation(gl, "Color2");
	        float[] colour2 = { col2R.getFloatValue(), col2G.getFloatValue(), col2B.getFloatValue() };
	        gl.glUniform3fv(colour2UniformLocation, 1, colour2, 0);
	        int avgColourUniformLocation = customShader.getUniformLocation(gl, "AvgColor");
	        float[] avgColour = { (colour1[0]+colour2[0])/2, (colour1[1]+colour2[1])/2, (colour1[2]+colour2[2])/2 };
	        gl.glUniform3fv(avgColourUniformLocation, 1, avgColour, 0);
	        int frequencyUniformLocation = customShader.getUniformLocation(gl, "Frequency");
	        double frequencyValue = frequency.getValue();
	        frequencyValue = useIntFrequency.getValue() ? Math.round(frequencyValue) : frequencyValue;
	        gl.glUniform1f(frequencyUniformLocation, (float) frequencyValue);
	        int useAveragingUniformLocation = customShader.getUniformLocation(gl, "UseAveraging");
	        gl.glUniform1i(useAveragingUniformLocation, useAveraging.getValue() ? 1 : 0);
	        int useSmoothStepUniformLocation = customShader.getUniformLocation(gl, "UseSmoothStep");
	        gl.glUniform1i(useSmoothStepUniformLocation, useSmoothStep.getValue() ? 1 : 0);
	        
	        // Woodcut uniforms
	        int timeUniformLocation = customShader.getUniformLocation(gl, "Time");
	        gl.glUniform1f(timeUniformLocation, time.getFloatValue());
	        int lightPositionUniformLocation = customShader.getUniformLocation(gl, "LightPosition");
	        float[] lightPosition = { lightPos.x, lightPos.y, lightPos.z };
	        gl.glUniform3fv(lightPositionUniformLocation, 1, lightPosition, 0);
	        
	        // Toon uniforms
	        int threshHighUniformLocation = customShader.getUniformLocation(gl, "ThresholdHigh");
	        gl.glUniform1f(threshHighUniformLocation, thresholdHigh.getFloatValue());
	        int threshMedUniformLocation = customShader.getUniformLocation(gl, "ThresholdMedium");
	        gl.glUniform1f(threshMedUniformLocation, thresholdMedium.getFloatValue());
	        int threshLowUniformLocation = customShader.getUniformLocation(gl, "ThresholdLow");
	        gl.glUniform1f(threshLowUniformLocation, thresholdLow.getFloatValue());
        }
		
        // Draw teapot
        gl.glMaterialfv( GL.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, new float[] {1,1,0,1}, 0 );
        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, new float[] {1,1,1,1}, 0 );
        gl.glMaterialf( GL.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 50 );
        glut.glutSolidTeapot(1);
        
        // Draw table
        gl.glMaterialfv( GL.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, new float[] {0,1,1,1}, 0 );
        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, new float[] {1,1,1,1}, 0 );
        gl.glMaterialf( GL.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 50 );
        gl.glPushMatrix();
        gl.glTranslated(0, -0.8, 0);
        gl.glBegin(GL2.GL_TRIANGLE_FAN);
        gl.glNormal3d(0, 1, 0);
        gl.glTexCoord2d(0, 0); gl.glVertex3d(-10, 0, -10 );
        gl.glTexCoord2d(0, 1); gl.glVertex3d(-10, 0, 10 );
        gl.glTexCoord2d(1, 1); gl.glVertex3d( 10, 0, 10 );
        gl.glTexCoord2d(1, 0); gl.glVertex3d( 10, 0, -10 );
        gl.glEnd();
        gl.glPopMatrix();
	}
	
	@Override
	public void dispose(GLAutoDrawable drawable) {
		// can let application exit dispose of our resources
	}
	
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		// glViewport already called by component		
	}
	
	private void addControls() {
		// Blinn-Phong tab
		VerticalFlowPanel blinnPhongSettings = new VerticalFlowPanel();
		blinnPhongSettings.setBorder( new TitledBorder("Shader Settings") );
		blinnPhongSettings.add(shininess.getSliderControls(false));
		blinnPhongControls.add(blinnPhongSettings.getPanel());
		ev.controlFrame.add(blinnPhongTabName, blinnPhongControls.getPanel());
		
		// Checkerboard tab
		VerticalFlowPanel checkerboardSettings = new VerticalFlowPanel();
		checkerboardSettings.setBorder( new TitledBorder("Shader Settings") );
		VerticalFlowPanel color1Panel = new VerticalFlowPanel();
		color1Panel.setBorder( new TitledBorder("Colour 1") );
		color1Panel.add(col1R.getSliderControls(false));
		color1Panel.add(col1G.getSliderControls(false));
		color1Panel.add(col1B.getSliderControls(false));
		checkerboardSettings.add(color1Panel.getPanel());
		VerticalFlowPanel color2Panel = new VerticalFlowPanel();
		color2Panel.setBorder( new TitledBorder("Colour 2") );
		color2Panel.add(col2R.getSliderControls(false));
		color2Panel.add(col2G.getSliderControls(false));
		color2Panel.add(col2B.getSliderControls(false));
		checkerboardSettings.add(color2Panel.getPanel());
		checkerboardSettings.add(frequency.getSliderControls(false));
		checkerboardSettings.add(useIntFrequency.getControls());
		checkerboardSettings.add(useAveraging.getControls());
		checkerboardSettings.add(useSmoothStep.getControls());
		checkerboardControls.add(checkerboardSettings.getPanel());
		ev.controlFrame.add(checkerboardTabName, checkerboardControls.getPanel());
		
		// Woodcut tab
		VerticalFlowPanel woodcutSettings = new VerticalFlowPanel();
		woodcutSettings.setBorder( new TitledBorder("Shader Settings") );
		woodcutSettings.add(time.getSliderControls(false));
		woodcutControls.add(woodcutSettings.getPanel());
		ev.controlFrame.add(woodcutTabName, woodcutControls.getPanel());
		
		// Toon tab
		VerticalFlowPanel toonSettings = new VerticalFlowPanel();
		toonSettings.setBorder( new TitledBorder("Shader Settings") );
		toonSettings.add(thresholdHigh.getSliderControls(false));
		toonSettings.add(thresholdMedium.getSliderControls(false));
		toonSettings.add(thresholdLow.getSliderControls(false));
		toonControls.add(toonSettings.getPanel());
		ev.controlFrame.add(toonTabName, toonControls.getPanel());
	}

	@Override
	public JPanel getControls() {

		blankPanel.add(new JLabel(" "));
		blankPanel.add(new JLabel("No custom shader is active, use the keys 1-4 to choose a shader.", SwingConstants.CENTER));
		blankPanel.add(new JLabel(" "));
		blankPanel.add(new JLabel("Shader choices:", SwingConstants.CENTER));
		blankPanel.add(new JLabel(" "));
		blankPanel.add(new JLabel("1) Blinn-Phong Fragment Shader", SwingConstants.CENTER));
		blankPanel.add(new JLabel(" "));
		blankPanel.add(new JLabel("2) Checkerbooard Shader", SwingConstants.CENTER));
		blankPanel.add(new JLabel(" "));
		blankPanel.add(new JLabel("3) Woodcut (Hashing) Shader", SwingConstants.CENTER));
		blankPanel.add(new JLabel(" "));
		blankPanel.add(new JLabel("4) Toon Shader", SwingConstants.CENTER));
		blankPanel.add(new JLabel(" "));
		
		lightControls.setBorder( new TitledBorder("Light Controls"));
		lightControls.add(lightPos);
		VerticalFlowPanel lightColorPanel = new VerticalFlowPanel();
		lightColorPanel.setBorder( new TitledBorder("Light Colour") );
		lightColorPanel.add(lightColR.getSliderControls(false));
		lightColorPanel.add(lightColG.getSliderControls(false));
		lightColorPanel.add(lightColB.getSliderControls(false));
		lightControls.add(lightColorPanel.getPanel());
		lightControls.add(ambient.getSliderControls(false));
		
		blankPanel.add(lightControls.getPanel());
        return blankPanel.getPanel();
	}
	
}
