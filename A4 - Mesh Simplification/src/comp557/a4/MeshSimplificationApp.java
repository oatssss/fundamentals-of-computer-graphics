package comp557.a4;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import javax.swing.JPanel;
import javax.vecmath.Vector4d;

import mintools.parameters.BooleanParameter;
import mintools.parameters.DoubleParameter;
import mintools.parameters.IntParameter;
import mintools.swing.VerticalFlowPanel;
import mintools.viewer.EasyViewer;
import mintools.viewer.Interactor;
import mintools.viewer.SceneGraphNode;

import com.jogamp.opengl.DebugGL2;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.util.gl2.GLUT;

/**
 * COMP557 - Mesh simplification application
 */
public class MeshSimplificationApp implements SceneGraphNode, Interactor {

    public static void main(String[] args) {
        new MeshSimplificationApp();
    }
    
    private PolygonSoup soup;
    
    private HEDS heds;
    
    private HalfEdge currentHE;
    
    private int whichSoup = 0;
    
    private String[] soupFiles = {
    		"meshdata/tetrahedron.obj",
    		"meshdata/topologytest.obj",
    		"meshdata/ico-sphere-tris.obj",
    		"meshdata/icosphere2.obj",
    		"meshdata/cube.obj",
    		"meshdata/cube2obj.obj",
    		"meshdata/bunny.obj",
    		"meshdata/cow.obj",    		
            "meshdata/monkey.obj",            
        };
    
    public MeshSimplificationApp() {    
        loadSoupBuildAndSubdivide( soupFiles[0] );
        EasyViewer ev = new EasyViewer("Comp 557 Mesh Simplification - OTHNIEL CUNDANGAN", this, new Dimension(400, 400), new Dimension(600, 400) );
        ev.addInteractor(this);
    }
    
    /** Priority queue of edges to collapse */
    PriorityQueue<Edge> collapseQueue = new PriorityQueue<Edge>();
    
    /**
     * Loads the currently 
     */
    private void loadSoupBuildAndSubdivide( String filename ) {          
        soup = new PolygonSoup( filename );
        heds = new HEDS( soup );
        if ( heds.faces.size() > 0 ) {
        	currentHE = heds.faces.get(0).he;
        }
        collapseQueue.clear();
        skippedCollapses.clear();
        heds.buildPriorityQueue(collapseQueue, regularizationWeight.getValue());
        currentHE = collapseQueue.peek().he;
        targetFaceCount.setMaximum(soup.faceList.size());
        targetFaceCount.setValue(soup.faceList.size()/2);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
//    	heds.buildPriorityQueue(collapseQueue, regularizationWeight.getValue());
        GL2 gl = drawable.getGL().getGL2();

        if ( ! cullFace.getValue() ) {             
        	gl.glDisable( GL.GL_CULL_FACE );
        } else {
        	gl.glEnable( GL.GL_CULL_FACE );
        }

        
        if ( !wireFrame.getValue()) {
            // if drawing with lighting, we'll set the material
            // properties for the font and back surfaces, and set
            // polygons to render filled.
            gl.glEnable(GL2.GL_LIGHTING);
            final float frontColour[] = {.7f,.7f,0,1};
            final float backColour[] = {0,.7f,.7f,1};
            final float[] shinyColour = new float[] {1f, 1f, 1f, 1};            
            gl.glEnable(GL2.GL_LIGHTING);
            gl.glMaterialfv( GL.GL_FRONT,GL2.GL_AMBIENT_AND_DIFFUSE, frontColour, 0 );
            gl.glMaterialfv( GL.GL_BACK,GL2.GL_AMBIENT_AND_DIFFUSE, backColour, 0 );
            gl.glMaterialfv( GL.GL_FRONT_AND_BACK,GL2.GL_SPECULAR, shinyColour, 0 );
            gl.glMateriali( GL.GL_FRONT_AND_BACK,GL2.GL_SHININESS, 50 );
            gl.glLightModelf(GL2.GL_LIGHT_MODEL_TWO_SIDE, 1);
            gl.glPolygonMode( GL.GL_FRONT_AND_BACK, GL2.GL_FILL );            
        } else {
            // if drawing without lighting, we'll set the colour to white
            // and set polygons to render in wire frame
            gl.glDisable( GL2.GL_LIGHTING );
            gl.glColor4f(.7f,.7f,0.0f,1);
            gl.glPolygonMode( GL.GL_FRONT_AND_BACK, GL2.GL_LINE );
        }    
        
        
        if ( drawHEDSMesh.getValue() ) heds.display( drawable );
        
        
        if ( drawPolySoup.getValue() ) {
            gl.glDisable(GL2.GL_LIGHTING);
            gl.glEnable( GL.GL_BLEND );
            gl.glBlendFunc( GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA );
            gl.glColor4f(.7f,.7f,7.0f,0.5f);
            gl.glLineWidth(1);
            gl.glPolygonMode( GL.GL_FRONT_AND_BACK, GL2.GL_LINE );
            soup.display( drawable );
            gl.glPolygonMode( GL.GL_FRONT_AND_BACK, GL2.GL_FILL );
        }
        
        if ( drawHalfEdge.getValue() && currentHE != null ) {
            currentHE.display( drawable );
        }
        
        gl.glColor4f(1,1,1,1);
        int logStringYPos;
        EasyViewer.beginOverlay(drawable);
        EasyViewer.printTextLines(drawable, soupFiles[whichSoup] + " Faces = " + heds.faces.size(), 10,20,15, GLUT.BITMAP_8_BY_13 );
        // Print instructions on screen
        if (showInstructions.getValue()) {
	        EasyViewer.printTextLines(drawable, "SPACE to select the highlighted edge's twin", 10, 40, 15, GLUT.BITMAP_HELVETICA_10);
	        EasyViewer.printTextLines(drawable, "N to select the edge's neighbour", 10, 55, 15, GLUT.BITMAP_HELVETICA_10);
	        EasyViewer.printTextLines(drawable, "G to select the best edge for collapsing and maintaining geometry", 10, 70, 15, GLUT.BITMAP_HELVETICA_10);
	        EasyViewer.printTextLines(drawable, "C to collapse the highlighted edge", 10, 85, 15, GLUT.BITMAP_HELVETICA_10);
	        EasyViewer.printTextLines(drawable, "V to decimate until the specified LOD", 10, 100, 15, GLUT.BITMAP_HELVETICA_10);
	        EasyViewer.printTextLines(drawable, "PAGE DOWN to load the next model (fn + down on mac)", 10, 115, 15, GLUT.BITMAP_HELVETICA_10);
	        EasyViewer.printTextLines(drawable, "PAGE UP to load the previous model (fn + up on mac)", 10, 130, 15, GLUT.BITMAP_HELVETICA_10);
	        EasyViewer.printTextLines(drawable, "These instructions can be toggled in the control panel", 10, 145, 15, GLUT.BITMAP_HELVETICA_10);
	        logStringYPos = 160;
        }
        else {
			logStringYPos = 40;
		}
        // Print the error log string
        gl.glColor3f(1,0,0);
        EasyViewer.printTextLines(drawable, logString, 10, logStringYPos, 15, GLUT.BITMAP_HELVETICA_10 );
        gl.glColor4f(1,1,1,1);
        EasyViewer.endOverlay(drawable);
        
        if (!logString.isEmpty()) {
			if ((System.currentTimeMillis() - logStringTimestampMilliseconds) > logStringDisplayTimeMilliseconds) {
				logString = "";
			}
		}
        
        Vector4d optimalPosition = new Vector4d();
        if (currentHE != null) {
        	Edge e = currentHE.e;
            e.recomputeQuadrics(regularizationWeight.getValue());
            optimalPosition = e.v;
		}
		
		// Actually draw the optimal position
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor4f(1,0,0,1);
		gl.glPointSize(10);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		gl.glBegin(GL2.GL_POINTS);
		gl.glVertex3d(optimalPosition.x, optimalPosition.y, optimalPosition.z);
		gl.glEnd();
		
		// Update the LOD controls
//		targetFaceCount.setDefaultValue(soup.faceList.size()/2);
		
		// Do the decimation if it was triggered
		if (doingDecimation) {
			if (heds.faces.size() <= targetFaceCount.getValue() || heds.faces.size() <= 4) {
				doingDecimation = false;
			} else {
				doDecimation();
			}
		}
    }
    
    // For showing errors as text in the easy viewer
    static private String logString = "";
	private static long logStringTimestampMilliseconds;
	private static int logStringDisplayTimeMilliseconds = 5000;
    public static void setLogString(String logString) {
		MeshSimplificationApp.logString = logString;
		logStringTimestampMilliseconds = System.currentTimeMillis();
	}
    
    @Override
    public void init(GLAutoDrawable drawable) {
        drawable.setGL( new DebugGL2(drawable.getGL().getGL2()) );
        GL2 gl = drawable.getGL().getGL2();
        gl.glEnable( GL.GL_BLEND );
        gl.glBlendFunc( GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA );
        gl.glEnable( GL.GL_LINE_SMOOTH );
        gl.glEnable( GL2.GL_POINT_SMOOTH );
        gl.glEnable( GL2.GL_NORMALIZE );
        gl.glShadeModel( GL2.GL_SMOOTH ); // Enable smooth shading, though everything should be flat!
    }

    private BooleanParameter drawPolySoup = new BooleanParameter( "draw soup mesh (wire frame)", true );    
    private BooleanParameter drawHEDSMesh = new BooleanParameter( "draw HEDS mesh", true );
    private BooleanParameter cullFace = new BooleanParameter( "cull face", true );
    private BooleanParameter wireFrame = new BooleanParameter( "wire frame", false );    
    private BooleanParameter drawHalfEdge = new BooleanParameter( "draw test half edge", true );    
    private DoubleParameter regularizationWeight = new DoubleParameter( "regularizaiton", 0.01, 1e-6, 1e2 );
    private BooleanParameter drawEdgeErrors = new BooleanParameter("draw edge errors", true );
    private IntParameter targetFaceCount = new IntParameter("Target Face Count (LOD)", 10, 4, 6000);
    private BooleanParameter showInstructions = new BooleanParameter("Display Instructions", true);

    
    @Override
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.add( drawPolySoup.getControls() );
        vfp.add( drawHEDSMesh.getControls() );                           
        vfp.add( cullFace.getControls() );
        vfp.add( wireFrame.getControls() );
        vfp.add( drawHalfEdge.getControls() );
        vfp.add( regularizationWeight.getSliderControls(true) );
        vfp.add( drawEdgeErrors.getControls() );
        vfp.add( targetFaceCount.getControls() );
        vfp.add( showInstructions.getControls() );
        return vfp.getPanel();
    }
    
    private List<Edge> skippedCollapses = new ArrayList<Edge>();
    private boolean doingDecimation = false;

    @Override
    public void attach(Component component) {
        component.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_SPACE && doingDecimation == false) {
                    if ( currentHE.twin != null ) currentHE = currentHE.twin;                    
                } else if (e.getKeyCode() == KeyEvent.VK_N && doingDecimation == false) {
                    if ( currentHE.next != null ) currentHE = currentHE.next;
                } else if ( e.getKeyCode() == KeyEvent.VK_PAGE_UP ) {
                	doingDecimation = false;
                    if ( whichSoup > 0 ) whichSoup--;                    
                    loadSoupBuildAndSubdivide( soupFiles[whichSoup] );
                } else if ( e.getKeyCode() == KeyEvent.VK_PAGE_DOWN ) {
                	doingDecimation = false;
                    if ( whichSoup < soupFiles.length -1 ) whichSoup++;                    
                    loadSoupBuildAndSubdivide( soupFiles[whichSoup] );
                } else if ( e.getKeyCode() == KeyEvent.VK_C && doingDecimation == false ) {
                	doDecimation();
                	
                	setLogString("Edges in queue: " + collapseQueue.size());
                } else if ( e.getKeyCode() == KeyEvent.VK_G && doingDecimation == false ) {
                	if (collapseQueue.peek() != null)
                		{ currentHE = collapseQueue.peek().he; }
                	
                } else if ( e.getKeyCode() == KeyEvent.VK_V ) {
                	currentHE = null;
                	if (collapseQueue.peek() != null) {
						currentHE = collapseQueue.peek().he;
					}
                	doingDecimation = true;
                }
            }
        });
    }
    
    private void doDecimation() {
    	// Do the collapse if possible
    	
    	if (currentHE == null) {
			return;
		}
    	if (heds.collapse(currentHE.e.v, currentHE, collapseQueue, skippedCollapses, regularizationWeight.getValue()))
    	{
    		collapseQueue.addAll(skippedCollapses);		// Add any skipped half edges back to the priority queue, they might be possible now
    		skippedCollapses.clear();					// We've added the skipped half edges back in, clear this queue
		}
    	else {
    		// Remove the highest priority edge and add it to the list of skipped edges since we can't collapse it
			skippedCollapses.add(collapseQueue.poll());
		}
    	
    	Edge first = collapseQueue.peek();
    	if (first != null) {
    		currentHE = first.he;			// Assign the next best candidate
		} else {
			currentHE = null;
		}
	}
}
