package comp557.a5;

import java.io.ObjectInputStream.GetField;
import java.util.regex.Matcher;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A simple box class. A box is defined by it's lower (@see min) and upper (@see max) corner.
 */
public class Box extends Intersectable {

	public Point3d max;
	public Point3d min;

	/**
	 * Default constructor. Creates a 2x2x2 box centered at (0,0,0)
	 */
	public Box() {
		super();
		this.max = new Point3d(1, 1, 1);
		this.min = new Point3d(-1, -1, -1);
	}

	@Override
	public void intersect(Ray ray, IntersectResult result) {
		
		Vector3d d = ray.viewDirection;

		//	Get the bounds of the box
		double xMin = this.min.x;
		double xMax = this.max.x;
		double yMin = this.min.y;
		double yMax = this.max.y;
		double zMin = this.min.z;
		double zMax = this.max.z;
		
		//	Get tMin and tMax of each axis
		double tX1 = (xMin - ray.eyePoint.x)/d.x;
		double tX2 = (xMax - ray.eyePoint.x)/d.x;
		double tY1 = (yMin - ray.eyePoint.y)/d.y;
		double tY2 = (yMax - ray.eyePoint.y)/d.y;
		double tZ1 = (zMin - ray.eyePoint.z)/d.z;
		double tZ2 = (zMax - ray.eyePoint.z)/d.z;
		
		//	Find the tMin of the axes relative to the ray origin
		double tMinX = Math.min(tX1, tX2);
		double tMinY = Math.min(tY1, tY2);
		double tMinZ = Math.min(tZ1, tZ2);
		double tMaxX = Math.max(tX1, tX2);
		double tMaxY = Math.max(tY1, tY2);
		double tMaxZ = Math.max(tZ1, tZ2);
		
		//	The normal will correspond to the axis that is hit first
		double tMin;
		Vector3d n = new Vector3d();
		if (tMinX > tMinY)
			{ n.set(1, 0, 0); tMin = tMinX; if (ray.eyePoint.x < xMin) n.negate(); }
		else
			{ n.set(0, 1, 0); tMin = tMinY; if (ray.eyePoint.y < yMin) n.negate(); }
		if (tMinZ > tMin)
			{ n.set(0, 0, 1); tMin = tMinZ; if (ray.eyePoint.z < zMin) n.negate(); }
		
		//	Finally get tMin and tMax overall
//		double tMin = Math.max(Math.max(tMinX, tMinY), tMinZ);
		double tMax = Math.min(Math.min(tMaxX, tMaxY), tMaxZ);
		
		//	We don't want negative t values, since those are behind the ray origin
		if (tMin < 0)
			{ return; }
		
		//	This intersection only matters if it's the closest object to the ray so far
		if (tMin > result.t)
			{ return; }
		
		if (tMax - tMin < 0)
			{ return; }
		
		double t = tMin;
		//	Calculate the actual intersection point using t
		Vector3d td = new Vector3d(d);
		td.scale(t);
		Point3d p = new Point3d(ray.eyePoint);
		p.add(td);
		
		// Update result
		result.n.set(n);
		result.p.set(p);
		result.t = t;
		result.material = this.material;
	}

}
