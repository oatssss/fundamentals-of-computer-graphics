package comp557.a2;

import javax.vecmath.Vector3d;

public class DAGGeoCube extends DAGGeometry {
	
	public DAGGeoCube ( String name, Vector3d offsetFromParent, float xRot, float yRot, float zRot, Vector3d scale, float[] drawColour ) {
		super(name, offsetFromParent, xRot, yRot, zRot, scale, drawColour);
	}
	
	public DAGGeoCube ( String name ) {
		super(name);
	}
	
	@Override
	protected void displaySpecificShape () {
		DAGNode.glut.glutSolidCube(1);
	}
}
