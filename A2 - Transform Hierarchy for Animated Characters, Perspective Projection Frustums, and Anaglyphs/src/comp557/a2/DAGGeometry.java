package comp557.a2;

import javax.vecmath.Vector3d;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

import mintools.parameters.DoubleParameter;

public abstract class DAGGeometry extends DAGNode {

//	gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, colours[0], 0 );
	
	public static float[] TURQUOISE = {.75f,1,1,1};
	public static float[] PURPLE = {1,.75f,1,1};
	public static float[] YELLOW = {1,1,.75f,1};
	public static float[] BLUE = {.75f,.75f,1,1};
	public static float[] PINK = {1,.75f,.75f,1};
	public static float[] BLACK = {0.25f,0.25f,0.25f,1};
	public static float[] GREEN = {.75f,1,.75f,1};
	float[] drawColour = BLACK;
	Vector3d offsetFromParent;
	Vector3d scale;
	float xRotation;
	float yRotation;
	float zRotation;
	DoubleParameter offsetX = new DoubleParameter("Offset X", 0, -10, 10);
	DoubleParameter offsetY = new DoubleParameter("Offset Y", 0, -10, 10);
	DoubleParameter offsetZ = new DoubleParameter("Offset Z", 0, -10, 10);
	DoubleParameter xRot = new DoubleParameter("X Rotation (Degrees)", 0, -180, 180);
	DoubleParameter yRot = new DoubleParameter("X Rotation (Degrees)", 0, -180, 180);
	DoubleParameter zRot = new DoubleParameter("X Rotation (Degrees)", 0, -180, 180);
	DoubleParameter xScale = new DoubleParameter("X Scale", 1, 0.01, 3);
	DoubleParameter yScale = new DoubleParameter("Y Scale", 1, 0.01, 3);
	DoubleParameter zScale = new DoubleParameter("Z Scale", 1, 0.01, 3);
	protected boolean isBeingPlaced = false;
	
	public DAGGeometry ( String name, Vector3d offsetFromParent, float xRot, float yRot, float zRot, Vector3d scale, float[] drawColour ) {
		this.name = name;
		this.offsetFromParent = offsetFromParent;
		this.xRotation = xRot;
		this.yRotation = yRot;
		this.zRotation = zRot;
		this.scale = scale;
		this.drawColour = drawColour;
	}
	
	public DAGGeometry ( String name ) {
		this(name, new Vector3d(0, 0, 0), 0, 0, 0, new Vector3d(1, 1, 1), BLACK);
		this.dofs.add(offsetX);
		this.dofs.add(offsetY);
		this.dofs.add(offsetZ);
		this.dofs.add(xRot);
		this.dofs.add(yRot);
		this.dofs.add(zRot);
		this.dofs.add(xScale);
		this.dofs.add(yScale);
		this.dofs.add(zScale);
		isBeingPlaced = true;
	}
	
	@Override
	public final void display( GLAutoDrawable drawable ) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glPushMatrix();
		
		if (isBeingPlaced) {
			gl.glTranslatef(offsetX.getFloatValue(), offsetY.getFloatValue(), offsetZ.getFloatValue());
			gl.glRotatef(xRot.getFloatValue(), 1, 0, 0);
			gl.glRotatef(yRot.getFloatValue(), 0, 1, 0);
			gl.glRotatef(zRot.getFloatValue(), 0, 0, 1);
			gl.glScalef(xScale.getFloatValue(), yScale.getFloatValue(), zScale.getFloatValue());
		}
		else {
			gl.glTranslatef((float)offsetFromParent.x, (float)offsetFromParent.y, (float)offsetFromParent.z);
			gl.glRotatef(xRotation, 1, 0, 0);
			gl.glRotatef(yRotation, 0, 1, 0);
			gl.glRotatef(zRotation, 0, 0, 1);
			gl.glScalef((float)scale.x, (float)scale.y, (float)scale.z);
		}
		
		gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, drawColour, 0 );
		displaySpecificShape();
		super.display(drawable);
		gl.glPopMatrix();
	}
	
	protected abstract void displaySpecificShape ();
}
