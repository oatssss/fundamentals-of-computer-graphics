package comp557.a5;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A simple sphere class.
 */
public class Sphere extends Intersectable {

	/**
	 * Radius of the sphere.
	 */
	public double radius;

	/**
	 * Location of the sphere center.
	 */
	public Point3d center;

	/**
	 * Default constructor. Creates a unit sphere centered at (0,0,0)
	 */
	public Sphere() {
		super();
		this.radius = 1.0;
		this.center = new Point3d(0,0,0);
	}

	/**
	 * Creates a sphere with the request radius and center.
	 *
	 * @param radius
	 * @param center
	 * @param material
	 */
	public Sphere(double radius, Point3d center, Material material) {
		this.radius = radius;
		this.center = center;
		this.material = material;
	}

	@Override
	public void intersect( Ray ray, IntersectResult result ) {

		Point3d pMinusCpoint = new Point3d();
		pMinusCpoint.sub(ray.eyePoint, this.center);
		Vector3d pMinusC = new Vector3d(pMinusCpoint);
		double pMcDotpMc = pMinusC.dot(pMinusC);
		double dDotd = ray.viewDirection.dot(ray.viewDirection);
		double dDotpMc = ray.viewDirection.dot(pMinusC);
		
		// Calculate the discriminant
		double discriminant = Math.pow(dDotpMc, 2) - dDotd*(pMcDotpMc - Math.pow(this.radius, 2));
		// Check if ray intersects
		if (discriminant >= 0) {
			// Use the formula for spherical intersections to find t, then the intersection point
			double t0 = (-dDotpMc - Math.sqrt(discriminant))/dDotd;
			double t1 = (-dDotpMc + Math.sqrt(discriminant))/dDotd;
			double t = t0 < t1 ? t0 : t1;
			Point3d td = new Point3d(ray.viewDirection);
			td.scale(t);
			Point3d p = new Point3d(ray.eyePoint);
			p.add(td);
			// Get the normal
			Vector3d n = new Vector3d();
			n.sub(p, this.center);
//			n.scale(1/this.radius);
			n.normalize();
//			System.out.println("NORMAL : " + n);

			
			
			// Only update the intersect result if this intersection point occurs earlier in the ray's trajectory
			if ( t < result.t ) {
				result.n.set(n);
				result.p.set(p);
				System.out.println("INTERSECTION : " + result.p);
				result.t = t;
				result.material = (this.material == null) ? result.material : this.material;
			}
		}
		
		// Otherwise, leave the ray unchanged
	}

}
