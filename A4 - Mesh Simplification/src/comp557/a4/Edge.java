package comp557.a4;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector4d;

/**
 * A class to store information concerning mesh simplificaiton
 * that is common to a pair of half edges.  Speicifically, 
 * the error metric, optimal vertex location on collapse, 
 * and the error.
 * @author kry
 */
public class Edge implements Comparable<Edge> {
	
	/** One of the two half edges */
	HalfEdge he;
	
	/** Optimal vertex location on collapse */
	Vector4d v = new Vector4d();
	
	/** Error metric for this edge */
	Matrix4d Q = new Matrix4d();
	
	/** The error involved in performing the collapse of this edge */
	double error;
	
	public void recomputeQuadrics(double regularizationWeight) {
		// Get the vertices associated with the half edge
		Vertex i = he.head;
		Vertex j = he.prev().head; // Use prev instead of twin because twin might not exist if the half edge is a boundary
		
		// Create the regularization matrix
		Matrix4d regularization = new Matrix4d();
		regularization.setIdentity();
		regularization.mul(regularizationWeight);

		// Sum the Qs from the edge's vertices along with the regularization term to get the edge's Q matrix
		Matrix4d q = new Matrix4d();
		q.setZero();
		q.add(i.Q);
		q.add(j.Q);
		this.Q = q;

		// Now find the optimal position
		Matrix4d qZeroBottom = new Matrix4d(q);
		qZeroBottom.add(regularization);
		qZeroBottom.setRow(3, 0, 0, 0, 1);	// v is an homogeneous vector, w component is always 1
		qZeroBottom.invert();				// Guarenteed possible because of regularization
		Vector4d optimalPosition = new Vector4d(qZeroBottom.m03, qZeroBottom.m13, qZeroBottom.m23, qZeroBottom.m33);
		this.v = optimalPosition;

		// Calculate the error associated with the optimal position
		double col_0 = optimalPosition.x*q.m00 + optimalPosition.y*q.m10 + optimalPosition.z*q.m20 + optimalPosition.w*q.m30;
		double col_1 = optimalPosition.x*q.m01 + optimalPosition.y*q.m11 + optimalPosition.z*q.m21 + optimalPosition.w*q.m31;
		double col_2 = optimalPosition.x*q.m02 + optimalPosition.y*q.m12 + optimalPosition.z*q.m22 + optimalPosition.w*q.m32;
		double col_3 = optimalPosition.x*q.m03 + optimalPosition.y*q.m13 + optimalPosition.z*q.m23 + optimalPosition.w*q.m33;
		double error = col_0*optimalPosition.x + col_1*optimalPosition.y + col_2*optimalPosition.z + col_3*optimalPosition.w;
		this.error = error;
	}
	
	@Override
	public int compareTo(Edge o) {
		if (error < o.error ) return -1;
		if (error > o.error ) return 1;
		return 0;
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Edge))
            { return false; }
        if (other == this)
            { return true; }
        
        Edge e = (Edge) other;
        boolean equal = false;
        if (he == e.he || he == e.he.twin)
        	{ equal = true; }
        
        return equal;
	}
	
	@Override
	public int hashCode() {
		
		int hashCode = he.hashCode();
		
		if (he.twin != null)
			{ hashCode += he.twin.hashCode(); }
		
		return hashCode;
	}
}
