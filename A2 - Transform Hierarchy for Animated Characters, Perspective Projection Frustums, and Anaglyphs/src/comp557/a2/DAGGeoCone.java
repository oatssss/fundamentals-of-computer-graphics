package comp557.a2;

import javax.vecmath.Vector3d;

public class DAGGeoCone extends DAGGeometry {
	
	public DAGGeoCone ( String name, Vector3d offsetFromParent, float xRot, float yRot, float zRot, Vector3d scale, float[] drawColour ) {
		super(name, offsetFromParent, xRot, yRot, zRot, scale, drawColour);
	}
	
	public DAGGeoCone ( String name ) {
		super(name);
	}
	
	@Override
	protected void displaySpecificShape () {
		DAGNode.glut.glutSolidCone(1, 1, 8, 8);
	}
}
