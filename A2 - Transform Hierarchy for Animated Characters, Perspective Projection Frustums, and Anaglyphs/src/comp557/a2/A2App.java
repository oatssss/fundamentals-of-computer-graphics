package comp557.a2;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector2f;

import mintools.parameters.BooleanParameter;
import mintools.parameters.DoubleParameter;
import mintools.parameters.Vec3Parameter;
import mintools.swing.ControlFrame;
import mintools.swing.VerticalFlowPanel;
import mintools.viewer.FlatMatrix4f;
import mintools.viewer.Interactor;
import mintools.viewer.TrackBallCamera;

import com.jogamp.opengl.DebugGL2;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;

/**
 * Assignment 2
 * @author YOUR NAME HERE
 */
public class A2App implements GLEventListener, Interactor {

	private String name = "Comp 557 Assignment 2 - OTHNIEL CUNDANGAN";
	
    /** Viewing mode as specified in the assignment */
    int viewingMode = 1;
    
    // some vector parameters to specify the viewing transformation for the head
    // (i.e., the coordinate frame between the two eyes)
    
	Vec3Parameter eye = new Vec3Parameter("eye (world space)", -0.4, 0.4, 1);
	Vec3Parameter lookAt = new Vec3Parameter("look at (world space)", 0, -0.07, 0);
	Vec3Parameter up = new Vec3Parameter("up (world space)", 0, 1, 0);

    // Note that limits the the numeric parameters are default, min, max, 
    // and the min and max might not be appropriate for what you are trying to do
    // so feel free to change them!
    
    private DoubleParameter eyeScreenDistance = new DoubleParameter( "eye screen distance", 1, 0.25, 3 ); 
    private DoubleParameter near = new DoubleParameter( "near", 0.1, 0.01, 1 ); 
    private DoubleParameter far  = new DoubleParameter( "far", 10, 0.02, 10 ); 
    
    private BooleanParameter drawCenterEyeFrustum = new BooleanParameter( "draw center eye frustum", true );    
    private BooleanParameter drawEyeFrustums = new BooleanParameter( "draw left and right eye frustums", true );
    
	/**
	 * The eye disparity should be constant, but can be adjusted to test the
	 * creation of left and right eye frustums or likewise, can be adjusted for
	 * your own eyes!! Note that 63 mm is a good inter occular distance for the
	 * average human, but you may likewise want to lower this to reduce the
	 * depth effect (images may be hard to fuse with cheap 3D colour filter
	 * glasses). Setting the disparity negative should help you check if you
	 * have your left and right eyes reversed!
	 */
    private DoubleParameter eyeDisparity = new DoubleParameter("eye disparity", 0.05, -0.1, 0.1 );

    private GLUT glut = new GLUT();
    private GLU glu = new GLU();
    
    private Scene scene = new Scene();

    /**
     * Launches the application
     * @param args
     */
    public static void main(String[] args) {
        new A2App();
    }
    
    GLCanvas glCanvas;
    
    /** Main trackball for viewing the world and the two eye frustums */
    TrackBallCamera tbc = new TrackBallCamera();
        
    /**
     * Creates the application
     */
    public A2App() {      
        Dimension controlSize = new Dimension(640, 640);
        Dimension size = new Dimension(800, 600);
        ControlFrame controlFrame = new ControlFrame("Controls");
        controlFrame.add("Camera", tbc.getControls());
        controlFrame.add("Scene", getControls());
        controlFrame.setSelectedTab("Scene");
        controlFrame.setSize(controlSize.width, controlSize.height);
        controlFrame.setLocation(size.width + 20, 0);
        controlFrame.setVisible(true);    
        GLProfile glp = GLProfile.getDefault();
        GLCapabilities glc = new GLCapabilities(glp);
        glCanvas = new GLCanvas( glc );
        glCanvas.setSize( size.width, size.height );
        glCanvas.setIgnoreRepaint( true );
        glCanvas.addGLEventListener( this );
        glCanvas.requestFocus();
        FPSAnimator animator = new FPSAnimator( glCanvas, 60 );
        animator.start();        
        tbc.attach( glCanvas );
        this.attach( glCanvas );        
        JFrame frame = new JFrame( name );
        frame.getContentPane().setLayout( new BorderLayout() );
        frame.getContentPane().add( glCanvas, BorderLayout.CENTER );
        frame.setLocation(0,0);        
        frame.addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                System.exit(0);
            }
        });
        frame.pack();
        frame.setVisible( true );        
    }
    
    @Override
    public void dispose(GLAutoDrawable drawable) {
    	// nothing to do
    }
        
    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        // Do nothing
    }
    
    @Override
    public void attach(Component component) {
        component.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() >= KeyEvent.VK_1 && e.getKeyCode() <= KeyEvent.VK_5) {
                    viewingMode = e.getKeyCode() - KeyEvent.VK_1 + 1;
                }           
            }
        });
    }
    
    /**
     * @return a control panel
     */
    public JPanel getControls() {     
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        vfp.add( eye);
        vfp.add( lookAt );
        vfp.add( up );
        vfp.add( eyeScreenDistance.getSliderControls(false));        
        vfp.add( near.getSliderControls(false));
        vfp.add( far.getSliderControls(false));        
        vfp.add( eyeDisparity.getSliderControls(false) ); 
        vfp.add ( drawCenterEyeFrustum.getControls() );
        vfp.add ( drawEyeFrustums.getControls() );        
        VerticalFlowPanel vfp2 = new VerticalFlowPanel();
        vfp2.setBorder( new TitledBorder("Scene controls" ));
        vfp2.add( scene.getControls() );
        vfp.add( vfp2.getPanel() );        
        return vfp.getPanel();
    }
             
    public void init( GLAutoDrawable drawable ) {
    	drawable.setGL( new DebugGL2( drawable.getGL().getGL2() ) );
        GL2 gl = drawable.getGL().getGL2();
        gl.glShadeModel(GL2.GL_SMOOTH);             // Enable Smooth Shading
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);    // Black Background
        gl.glClearDepth(1.0f);                      // Depth Buffer Setup
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glEnable(GL2.GL_POINT_SMOOTH);
        gl.glEnable(GL2.GL_NORMALIZE );
        gl.glEnable(GL.GL_DEPTH_TEST);              // Enables Depth Testing
        gl.glDepthFunc(GL.GL_LEQUAL);               // The Type Of Depth Testing To Do 
        gl.glLineWidth( 2 );                        // slightly fatter lines by default!
    }   

	double screenWidthPixels = 1440;
	double screenWidthMeters = 2;
	double metersPerPixel = screenWidthMeters / screenWidthPixels;
	
	// These are custom fields I've added to complete the assignment
    final float[] red = {1,0,0,1};
    final float[] green = {0,1,0,1};
    final float[] blue = {0,0,1,1};
    final float[] cyan = {0,1,1,1};
    final float[] white = {1, 1, 1, 1};
	
	private void drawAxes(GL2 gl) {
		
        gl.glPushMatrix();
        gl.glScalef(0.1f, 0.1f, 0.1f);
        
        gl.glDisable(GL2.GL_LIGHTING);		
		gl.glBegin( GL2.GL_LINES );
		
		// Draw x-axis
        gl.glColor3fv(red, 0);
        gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(1f, 0, 0);
		// Draw y-axis
        gl.glColor3fv(green, 0);
        gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(0, 1f, 0);
		// Draw z-axis
        gl.glColor3fv(blue, 0);
        gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(0, 0, 1f);
        
        gl.glEnd();        
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glPopMatrix();
	}
	
	private void transformToCameraFrame (GL2 gl, float x, float y, float z, float lookX, float lookY, float lookZ, float upX, float upY, float upZ) {
		
		gl.glPushMatrix();
        gl.glLoadIdentity();
        glu.gluLookAt(x, y, z, lookX, lookY, lookZ, upX, upY, upZ);
        float[] lookAtTransform = new float[16];
        gl.glGetFloatv(GL2.GL_MODELVIEW, lookAtTransform, 0);
        Matrix4f lookAtTransformMatrix = new Matrix4f(lookAtTransform);
        lookAtTransformMatrix.transpose();
        lookAtTransformMatrix.invert();	// Can run into a problem if matrix is not invertible
        FlatMatrix4f lookAtTransformFlat = new FlatMatrix4f(lookAtTransformMatrix);
        gl.glPopMatrix();
        gl.glMultMatrixf(lookAtTransformFlat.asArray(), 0);
	}
	
	private void drawRectangle (GL2 gl, Vector2f bottomLeft, Vector2f bottomRight, Vector2f topRight, Vector2f topLeft) {
		
		gl.glDisable(GL2.GL_LIGHTING);
        gl.glBegin(GL2.GL_LINE_LOOP);
        gl.glColor3fv(white, 0);
        gl.glVertex2f(bottomLeft.x, bottomLeft.y);
        gl.glVertex2f(bottomRight.x, bottomRight.y);
        gl.glVertex2f(topRight.x, topRight.y);
        gl.glVertex2f(topLeft.x, topRight.y);
        gl.glEnd();
        gl.glEnable(GL2.GL_LIGHTING);
	}
	
	private void drawFrustum (GL2 gl, float frustumLeft, float frustumRight, float frustumBottom, float frustumTop) {
		
		gl.glPushMatrix();
    	gl.glLoadIdentity();
    	gl.glFrustum (frustumLeft, frustumRight, frustumBottom, frustumTop, near.getFloatValue(), far.getFloatValue());
    	float[] frustumTransform = new float[16];
    	gl.glGetFloatv(GL2.GL_MODELVIEW, frustumTransform, 0);
    	gl.glPopMatrix();
    	Matrix4f frustumTransformMatrix = new Matrix4f(frustumTransform);
    	frustumTransformMatrix.transpose();
    	frustumTransformMatrix.invert();
    	FlatMatrix4f frustumTransformFlat = new FlatMatrix4f(frustumTransformMatrix);
    	gl.glPushMatrix();
    	gl.glMultMatrixf(frustumTransformFlat.asArray(), 0);
    	glut.glutWireCube(2f);
    	gl.glPopMatrix();
	}
    
    @Override
    public void display(GLAutoDrawable drawable) {        
        GL2 gl = drawable.getGL().getGL2();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); 
//        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
//        gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

        double w = drawable.getSurfaceWidth() * metersPerPixel;
        double h = drawable.getSurfaceHeight() * metersPerPixel;
        
        // Determine the frustum from the parameters
        float nearRatio = near.getFloatValue() / eyeScreenDistance.getFloatValue();
        float frustumWidth = nearRatio * (float) w;
        float frustumHeight = nearRatio * (float) h;
        float frustumLeft = -frustumWidth/2f;
        float frustumRight = frustumWidth/2f;
        float frustumBottom = -frustumHeight/2f;
        float frustumTop = frustumHeight/2f;
        
        if ( viewingMode == 1 ) {
        	// We will use a trackball camera, but also apply an 
        	// arbitrary scale to make the scene and frustums a bit easier to see
            tbc.prepareForDisplay(drawable);
            gl.glScaled( 15, 15, 15 );
            
            // Move origin to camera's origin
            gl.glMatrixMode( GL2.GL_MODELVIEW );
            gl.glPushMatrix();
            transformToCameraFrame(gl, eye.x, eye.y, eye.z, lookAt.x, lookAt.y, lookAt.z, up.x, up.y, up.z);
            
            // Draw the camera axis
            drawAxes(gl);
            
            // Draw the white screen rectangle
            gl.glPushMatrix();
            gl.glTranslatef(0, 0, -eyeScreenDistance.getFloatValue());
            
            // Determine the corners of the screen rectangle
            Vector2f bottomLeft = new Vector2f(-(float)w/2f, -(float)h/2f);
            Vector2f bottomRight = new Vector2f((float)w/2f, -(float)h/2f);
            Vector2f topRight = new Vector2f((float)w/2f, (float)h/2f);
            Vector2f topLeft = new Vector2f(-(float)w/2f, (float)h/2f);
            
            // Draw the rectangle
            drawRectangle(gl, bottomLeft, bottomRight, topRight, topLeft);
            
            gl.glPopMatrix();
            
            gl.glDisable(GL2.GL_LIGHTING);
            
            if (drawCenterEyeFrustum.getValue()) {
            	gl.glColor3fv(white, 0);
            	drawFrustum(gl, frustumLeft, frustumRight, frustumBottom, frustumTop);
            }
            
            // Pop out of center camera's coordinates
            gl.glPopMatrix();
            
            if (drawEyeFrustums.getValue()) {
            	
            	// Transform to the center camera's coordinates to get x-displacements local to the camera
            	gl.glPushMatrix();
            	transformToCameraFrame(gl, eye.x, eye.y, eye.z, lookAt.x, lookAt.y, lookAt.z, up.x, up.y, up.z);
            	
            	// First we'll do the left eye
            	gl.glPushMatrix();
            	// Transform to a camera coordinate just left of the center camera
            	transformToCameraFrame(gl, -eyeDisparity.getFloatValue()/2f, 0, 0, -eyeDisparity.getFloatValue()/2f, 0, -1, 0, 1, 0);
            	gl.glColor3fv(red, 0);
            	glut.glutSolidSphere(0.0125, 8, 8);
            	drawFrustum(gl, frustumLeft + eyeDisparity.getFloatValue()/2f * nearRatio, frustumRight + eyeDisparity.getFloatValue()/2f * nearRatio, frustumBottom, frustumTop);
            	// Pop out of the left eye's coordinates
            	gl.glPopMatrix();
            	
            	// Now the right eye
            	gl.glPushMatrix();
            	// Transform to a camera coordinate just right of the center camera
            	transformToCameraFrame(gl, +eyeDisparity.getFloatValue()/2f, 0, 0, +eyeDisparity.getFloatValue()/2f, 0, -1, 0, 1, 0);
            	gl.glColor3fv(cyan, 0);
            	glut.glutSolidSphere(0.0125, 8, 8);
            	drawFrustum(gl, frustumLeft - eyeDisparity.getFloatValue()/2f * nearRatio, frustumRight - eyeDisparity.getFloatValue()/2f * nearRatio, frustumBottom, frustumTop);
            	// Pop out of the left eye's coordinates
            	gl.glPopMatrix();
            	
            	// Pop out of the center camera's coordinates
            	gl.glPopMatrix();
            }
            
            gl.glEnable(GL2.GL_LIGHTING);
            scene.display( drawable );
            
        } else if ( viewingMode == 2 ) {

        	gl.glMatrixMode( GL2.GL_PROJECTION );
        	gl.glLoadIdentity();
        	gl.glFrustum (frustumLeft, frustumRight, frustumBottom, frustumTop, near.getFloatValue(), far.getFloatValue());
        	
        	gl.glMatrixMode( GL2.GL_MODELVIEW );
        	gl.glLoadIdentity();
        	glu.gluLookAt( eye.x, eye.y, eye.z, lookAt.x, lookAt.y, lookAt.z, up.x, up.y, up.z );        	

    		scene.display( drawable );
    		
        } else if ( viewingMode == 3 ) {
        	
        	gl.glMatrixMode( GL2.GL_PROJECTION );
        	gl.glLoadIdentity();
        	gl.glFrustumf(frustumLeft + eyeDisparity.getFloatValue()/2f * nearRatio, frustumRight + eyeDisparity.getFloatValue()/2f * nearRatio, frustumBottom, frustumTop, near.getFloatValue(), far.getFloatValue());
        	
        	gl.glMatrixMode( GL2.GL_MODELVIEW );
        	gl.glLoadIdentity();
        	// Transform view to center camera, then offset to left eye with another gluLookAt
        	glu.gluLookAt(eye.x, eye.y, eye.z, lookAt.x, lookAt.y, lookAt.z, up.x, up.y, up.z);
        	glu.gluLookAt(-eyeDisparity.getFloatValue()/2f, 0, 0, -eyeDisparity.getFloatValue()/2f, 0, -1, 0, 1, 0);     	

    		scene.display( drawable );
        	
        } else if ( viewingMode == 4 ) {  
        	
        	gl.glMatrixMode( GL2.GL_PROJECTION );
        	gl.glLoadIdentity();
        	gl.glFrustumf(frustumLeft - eyeDisparity.getFloatValue()/2f * nearRatio, frustumRight - eyeDisparity.getFloatValue()/2f * nearRatio, frustumBottom, frustumTop, near.getFloatValue(), far.getFloatValue());
        	
        	gl.glMatrixMode( GL2.GL_MODELVIEW );
        	gl.glLoadIdentity();
        	glu.gluLookAt(eye.x, eye.y, eye.z, lookAt.x, lookAt.y, lookAt.z, up.x, up.y, up.z);
        	glu.gluLookAt(+eyeDisparity.getFloatValue()/2f, 0, 0, +eyeDisparity.getFloatValue()/2f, 0, -1, 0, 1, 0);   
        	                               
        	scene.display( drawable );
        	
        } else if ( viewingMode == 5 ) {                    	
        	        	        	
        	gl.glMatrixMode( GL2.GL_MODELVIEW );
        	gl.glLoadIdentity();
        	// Transform view to center camera, then offset to left eye with another gluLookAt
        	glu.gluLookAt(eye.x, eye.y, eye.z, lookAt.x, lookAt.y, lookAt.z, up.x, up.y, up.z);
        	
        	// Render left eye
        	gl.glMatrixMode( GL2.GL_PROJECTION );
        	gl.glPushMatrix();
        	gl.glLoadIdentity();
        	gl.glFrustumf(frustumLeft + eyeDisparity.getFloatValue()/2f * nearRatio, frustumRight + eyeDisparity.getFloatValue()/2f * nearRatio, frustumBottom, frustumTop, near.getFloatValue(), far.getFloatValue());
        	gl.glMatrixMode( GL2.GL_MODELVIEW );
        	gl.glPushMatrix();
        	glu.gluLookAt(-eyeDisparity.getFloatValue()/2f, 0, 0, -eyeDisparity.getFloatValue()/2f, 0, -1, 0, 1, 0);     	
//        	gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        	gl.glColorMask(true, false, false, true);
        	scene.display( drawable );
    		gl.glPopMatrix();
    		gl.glMatrixMode( GL2.GL_PROJECTION );
        	gl.glPopMatrix();
    		
        	gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
        	
        	// Render right eye
        	gl.glMatrixMode( GL2.GL_PROJECTION );
        	gl.glPushMatrix();
        	gl.glLoadIdentity();
        	gl.glFrustumf(frustumLeft - eyeDisparity.getFloatValue()/2f * nearRatio, frustumRight - eyeDisparity.getFloatValue()/2f * nearRatio, frustumBottom, frustumTop, near.getFloatValue(), far.getFloatValue());
        	gl.glMatrixMode( GL2.GL_MODELVIEW );
        	gl.glPushMatrix();
        	glu.gluLookAt(+eyeDisparity.getFloatValue()/2f, 0, 0, +eyeDisparity.getFloatValue()/2f, 0, -1, 0, 1, 0);     	
        	gl.glColorMask(false, true, true, true);
//        	gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
        	scene.display( drawable );
    		gl.glPopMatrix();
    		gl.glMatrixMode( GL2.GL_PROJECTION );
        	gl.glPopMatrix();

        	gl.glColorMask(true, true, true, true);
        }        
    }
    
}
