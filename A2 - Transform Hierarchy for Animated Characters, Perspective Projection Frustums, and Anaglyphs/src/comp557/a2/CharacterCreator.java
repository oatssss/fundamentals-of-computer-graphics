package comp557.a2;

import javax.vecmath.Vector3d;


public class CharacterCreator {

	static public String name = "CHARACTER NAME - YOUR NAME AND STUDENT NUMBER";
	
	static public DAGNode create() {
		
		DAGJointFree root = new DAGJointFree("Root");
		DAGJointBall leftUpperArm = new DAGJointBall("Upper Left Arm", new Vector3d(0.28, 0.2, 0));
		DAGJointBall leftUpperLeg = new DAGJointBall("Upper Left Leg", new Vector3d(0.16, -0.45, 0));
		DAGJointHinge leftLowerArm = new DAGJointHinge("Lower Left Arm", new Vector3d(0, -0.6, 0), -150, 10);
		DAGJointHinge leftLowerLeg = new DAGJointHinge("Lower Left Leg", new Vector3d(0, -0.65, 0), -10, 150);
		DAGJointBall rightUpperArm = new DAGJointBall("Upper Right Arm", new Vector3d(-0.28, 0.2, 0));
		DAGJointBall rightUpperLeg = new DAGJointBall("Upper Right Leg", new Vector3d(-0.16, -0.45, 0));
		DAGJointHinge rightLowerArm = new DAGJointHinge("Lower Right Arm", new Vector3d(0, -0.6, 0), -150, 10);
		DAGJointHinge rightLowerLeg = new DAGJointHinge("Lower Right Leg", new Vector3d(0, -0.65, 0), -10, 150);
		DAGJointHinge head = new DAGJointHinge("Head", new Vector3d(0, 0.55, 0), -50, 50);
		
		DAGGeoCube torsoGeo = new DAGGeoCube("Torso Geometry", new Vector3d(0, 0, 0), 0, 0, 0, new Vector3d(0.6, 1.1, 0.5), DAGGeometry.PURPLE);
		DAGGeoCube leftUpperArmGeo = new DAGGeoCube("Upper Left Arm Geometry", new Vector3d(0, -0.26, 0), 0, 0, 0, new Vector3d(0.2, 0.8, 0.2), DAGGeometry.PURPLE);
		DAGGeoCube rightUpperArmGeo = new DAGGeoCube("Upper Right Arm Geometry", new Vector3d(0, -0.26, 0), 0, 0, 0, new Vector3d(0.2, 0.8, 0.2), DAGGeometry.PURPLE);
		DAGGeoCube leftUpperLegGeo = new DAGGeoCube("Upper Left Leg Geometry", new Vector3d(0, -0.36, 0), 0, 0, 0, new Vector3d(0.25, 0.7, 0.25), DAGGeometry.PURPLE);
		DAGGeoCube righttUpperLegGeo = new DAGGeoCube("Upper Right Leg Geometry", new Vector3d(0, -0.36, 0), 0, 0, 0, new Vector3d(0.25, 0.7, 0.25), DAGGeometry.PURPLE);
		DAGGeoCube headGeo = new DAGGeoCube("Head Geometry", new Vector3d(0, 0.35, 0), 0, 0, 0, new Vector3d(1, 1, 1), DAGGeometry.PURPLE);
		DAGGeoCone leftLowerArmGeo = new DAGGeoCone("Lower Left Arm Geometry", new Vector3d(0, 0, 0), 90, 0, 0, new Vector3d(0.1, 0.1, 0.7), DAGGeometry.PURPLE);
		DAGGeoCone rightLowerArmGeo = new DAGGeoCone("Lower Right Arm Geometry", new Vector3d(0, 0, 0), 90, 0, 0, new Vector3d(0.1, 0.1, 0.7), DAGGeometry.PURPLE);
		DAGGeoCone leftLowerLegGeo = new DAGGeoCone("Lower Left Leg Geometry", new Vector3d(0, 0, 0), 90, 0, 0, new Vector3d(0.1, 0.1, 0.7), DAGGeometry.PURPLE);
		DAGGeoCone rightLowerLegGeo = new DAGGeoCone("Lower Right Leg Geometry", new Vector3d(0, 0, 0), 90, 0, 0, new Vector3d(0.1, 0.1, 0.7), DAGGeometry.PURPLE);

		root.add(torsoGeo);
		root.add(head);
		root.add(leftUpperArm);
		root.add(rightUpperArm);
		root.add(leftUpperLeg);
		root.add(rightUpperLeg);
		
		head.add(headGeo);
		
		leftUpperArm.add(leftUpperArmGeo);
		leftUpperArm.add(leftLowerArm);
		leftLowerArm.add(leftLowerArmGeo);
		
		rightUpperArm.add(rightUpperArmGeo);
		rightUpperArm.add(rightLowerArm);
		rightLowerArm.add(rightLowerArmGeo);
		
		leftUpperLeg.add(leftUpperLegGeo);
		leftUpperLeg.add(leftLowerLeg);
		leftLowerLeg.add(leftLowerLegGeo);
		
		rightUpperLeg.add(righttUpperLegGeo);
		rightUpperLeg.add(rightLowerLeg);
		rightLowerLeg.add(rightLowerLegGeo);
		
		return root;
	}
}
