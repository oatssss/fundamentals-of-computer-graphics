// Original idea taken from http://www.lighthouse3d.com/tutorials/glsl-12-tutorial/toon-shading-version-iii/
// however, the shader code did not work properly because of incorrect lambertian calculations,
// and also did not work based on the light or material's colour,
// so I modified the shader to actually work like expected.

uniform float ThresholdHigh;
uniform float ThresholdMedium;
uniform float ThresholdLow;

varying vec3 normal;
varying vec3 vertexPos;

void main()
{
	float intensity;
	vec4 color;
	vec3 n = normalize(normal);
	vec3 lightVector = normalize( gl_LightSource[0].position.xyz - vertexPos ); // vector direction towards light
	float lambertian = max( 0.0, dot( n, lightVector ) );

	if (lambertian > ThresholdHigh)
		color = vec4(vec3(gl_FrontMaterial.diffuse * 1.0 * gl_LightSource[0].diffuse), 1.0);
	else if (lambertian > ThresholdMedium)
		color = vec4(vec3(gl_FrontMaterial.diffuse * ThresholdHigh * gl_LightSource[0].diffuse), 1.0);
	else if (lambertian > ThresholdLow)
		color = vec4(vec3(gl_FrontMaterial.diffuse * ThresholdMedium * gl_LightSource[0].diffuse), 1.0);
	else
		color = vec4(vec3(gl_FrontMaterial.diffuse * ThresholdLow * gl_LightSource[0].diffuse), 1.0);
	gl_FragColor = color;
}