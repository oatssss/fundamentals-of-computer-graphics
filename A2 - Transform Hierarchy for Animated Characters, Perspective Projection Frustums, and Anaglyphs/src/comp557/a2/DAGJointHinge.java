package comp557.a2;

import javax.vecmath.Vector3d;

import mintools.parameters.DoubleParameter;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

public class DAGJointHinge extends DAGNode {
	
	Vector3d offsetFromParent;
	DoubleParameter offsetX = new DoubleParameter("Offset X", 0, -10, 10);
	DoubleParameter offsetY = new DoubleParameter("Offset Y", 0, -10, 10);
	DoubleParameter offsetZ = new DoubleParameter("Offset Z", 0, -10, 10);
	DoubleParameter hingeRotation = new DoubleParameter("Hinge Rotation (Degrees)", 0, -180, 180);
	private boolean isBeingPlaced = false;
	
	/**
     * Creates a hinge node that allows rotation around a fixed axis.
     * The fixed axis is determined by orienting the hinge with a rotation around the y-axis,
     * then the actual rotation of the hinge is specified in the x-axis
     * @param name
     * 	The hinge's name
     * @param offsetFromParent
     * 	Where the hinge is located with respect to its parent
     */
	public DAGJointHinge ( String name, Vector3d offsetFromParent, float minRotation, float maxRotation ) {
		this.name = name;
		this.dofs.add(this.hingeRotation);
		this.offsetFromParent = offsetFromParent;
		this.hingeRotation.setMinimum((double)minRotation);
		this.hingeRotation.setMaximum((double)maxRotation);
	}
	
	public DAGJointHinge ( String name ) {
		this(name, new Vector3d(0, 0, 0), -180, 180);
		this.dofs.add(this.offsetX);
		this.dofs.add(this.offsetY);
		this.dofs.add(this.offsetZ);
		isBeingPlaced = true;
	}
	
	@Override
	public void display( GLAutoDrawable drawable ) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glPushMatrix();
		
		if (isBeingPlaced) {
			gl.glTranslatef(offsetX.getFloatValue(), offsetY.getFloatValue(), offsetZ.getFloatValue());
		}
		else {
			gl.glTranslatef((float)offsetFromParent.x, (float)offsetFromParent.y, (float)offsetFromParent.z);
		}
		
		gl.glRotatef(hingeRotation.getFloatValue(), 1, 0, 0);
		super.display(drawable);
		gl.glPopMatrix();
	}
}
