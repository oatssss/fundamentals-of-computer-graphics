One thing to note is that for the checkerboard shader,
because of how the glut teapot's texture coordinates are mapped,
if the frequency is a non-integer value, uneven tiling occurs.
I've added an extra checkbox to enable/disable rounding the frequency
to the nearest integer.

I also get this bug that prevents the 3D viewer from opening properly on launch.
Restarting the program usually fixes the issue, but sometimes it'll happen a second time.