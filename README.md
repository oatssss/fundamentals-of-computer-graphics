# Fundamentals of Computer Graphics
## COMP557 - Fundamentals of Computer Graphics

This is the Eclipse workspace holding all the source files for the assignments finished throughout the McGill course. Check the [releases](https://github.com/oatssss/Fundamentals-of-Computer-Graphics/releases) tab for runnable JARs showing off each assignment.
