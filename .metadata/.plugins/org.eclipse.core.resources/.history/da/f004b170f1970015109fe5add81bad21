package comp557.a5;

import java.awt.Dimension;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Simple camera object, which could be extended to handle a variety of
 * different camera settings (e.g., aperature size, lens, shutter)
 */
public class Camera {

	/** Camera name */
	public String name = "camera";

	/** The eye position */
	public Point3d from = new Point3d(0,0,10);

	/** The "look at" position */
	public Point3d to = new Point3d(0,0,0);

	/** Up direction, default is y up */
	public Vector3d up = new Vector3d(0,1,0);

	/** Vertical field of view (in degrees), default is 45 degrees */
	public double fovy = 45.0;

	/** The rendered image size */
	public Dimension imageSize = new Dimension(640,480);
	
	/** The transformation matrix that can be applied to points and vectors to get they're representation in world space */
	private Matrix4d localToWorld = null;
	
	/**
	 * Default constructor
	 */
	public Camera() {
		// do nothing
	}
	
	public Matrix4d localToWorldTransformMatrix() {
		
		// Either calculate or pull from cache
		if (localToWorld == null) {
			Matrix4d transform = new Matrix4d();
			
			/* Find the world space rotation of the camera as a rotation matrix */
			// 	First get the direction the camera is looking
			Vector3d lookDirection = new Vector3d();
			lookDirection.sub(this.from, this.to);
			lookDirection.normalize();
			
			Vector3d rotCol0 = new Vector3d();
			Vector3d rotCol1 = new Vector3d();
			Vector3d rotCol2 = new Vector3d();
			
			Vector3d xAxis = new Vector3d();
			xAxis.cross(this.up, lookDirection);
			xAxis.normalize();
			
			Vector3d yAxis = new Vector3d();
			yAxis.cross(lookDirection, xAxis);
			yAxis.normalize();
			
			rotCol0.set(xAxis.x, yAxis.x, lookDirection.x);
			rotCol1.set(xAxis.y, yAxis.y, lookDirection.y);
			rotCol2.set(xAxis.z, yAxis.z, lookDirection.z);
			
			Matrix3d cameraRot = new Matrix3d();
			cameraRot.setColumn(0, rotCol0);
			cameraRot.setColumn(1, rotCol1);
			cameraRot.setColumn(2, rotCol2);
			
			/* Set up the final transform matrix */
			transform.setRotationScale(cameraRot);
			transform.setTranslation(new Vector3d(this.from));
			
			localToWorld = transform;
			return localToWorld;
		}
		else {
			return localToWorld;
		}
	}
}
