Use the keys 1-5 to choose between different view modes:
  1) Free-view
  2) Camera view (Center)
  3) Camera view (Left Eye)
  4) Camera view (Right Eye)
  5) Camera view (Red-Cyan Anaglyph)

Additionally, you can choose to load an animation for the character by hitting the 'Load' button.
To play the animation, check the 'animate'.